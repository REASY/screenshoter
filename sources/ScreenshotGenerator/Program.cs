﻿using Screenshoter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ScreenshotGenerator
{
   class Program
   {
      static void Main(string[] args)
      {
         AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;
         try
         {
            Settings stg = new Settings();
            //Settings.Save(stg, "ScreenshoterService.config");
            Console.WriteLine("Starting server...");
            Server serv = new Server(stg, false);
            serv.Start();
            //serv.StartDebug();
            //serv.StartDebug2();
            Console.WriteLine("Server started.");
            Console.WriteLine("Press 'Enter' to quit.");
            Console.ReadLine();
            serv.Stop();
            Console.ReadLine();

         }
         catch(Exception ex)
         {
            Console.WriteLine("Failed to start server: {0}", ex.Message);
            Console.ReadLine();
         }
         
      }

      static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
      {
         
      }
      static void serv_ServerErrorEvent(object sender, Exception ex)
      {
         Console.WriteLine("While try start server an error occured: {0}", ex.Message);
         Console.ReadLine();
      }
   }
}
