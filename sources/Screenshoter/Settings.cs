﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Screenshoter
{
   [Serializable]
   public class Settings
   {
      /// <summary>
      /// TCP port on which the service listen to commands
      /// </summary>
      public int Port { get;  set; }
      /// <summary>
      /// Full path of the Log4Net configuration file
      /// </summary>
      public string LoggerCfgPath { get;  set; }
      /// <summary>
      /// Expected number of concurrent screenshots
      /// </summary>
      public int Capacity { get;  set; }
      /// <summary>
      /// Maximum number of redirections to follow
      /// </summary>
      public int Redirections { get;  set; }
      /// <summary>
      /// Maximum number of seconds to wait
      /// </summary>
      public int Timeout { get;  set; }

      public override string ToString()
      {
         string str = string.Format("Port: {0}, Capacity: {1}, Redirections: {2}, Timeout: {3} secs, LoggerCfgPath: '{4}'", Port, Capacity, Redirections, Timeout, LoggerCfgPath);
         return str;
      }
      public Settings()
      {
         Port = 12345;
         LoggerCfgPath = @"D:\Work\Elance\jusob\info\log2net.conf";
         Capacity = 10;
         Redirections = 10;
         Timeout = 180;
      }
      public static Settings Parse(string path)
      {
         Settings set = null;
         System.Xml.Serialization.XmlSerializer x = new System.Xml.Serialization.XmlSerializer(typeof(Settings));
         using (System.IO.TextReader txtReader = File.OpenText(path))
         {
            set = x.Deserialize(txtReader) as Settings;
         }
         return set;
      }
      public static void Save(Settings set, string path)
      {
         System.Xml.Serialization.XmlSerializer x = new System.Xml.Serialization.XmlSerializer(typeof(Settings));
         using (System.IO.FileStream fs = File.OpenWrite(path))
         {
            x.Serialize(fs, set);
         }
      }
   }
}
