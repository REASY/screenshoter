using System;
using System.Text;
using System.Runtime.InteropServices;
using mshtml; // Microsoft.mshtml primary Interop assembly

/*********************************************************************/

partial class WebShot : IDisposable
{
   public delegate void CaptureDelegate(object sender, IntPtr hBitmap, ref int saveToFile);
   public delegate void DocumentDelegate(object sender, [In, MarshalAs(UnmanagedType.Interface)] IHTMLDocument2 document);
   public delegate void StatusDelegate(object sender, string status);
   public delegate void ExitDelegate(object sender);

   public event CaptureDelegate CaptureEvent;
   public event DocumentDelegate DocumentEvent;
   public event StatusDelegate StatusEvent;
   public event ExitDelegate ExitEvent;

   private bool _isDisposed = false;
   private volatile IntPtr _ws = IntPtr.Zero;
   private bool isDllInited = false;
   private bool _isOleInited = false;
   private bool _isCreated = false;
   private WebShot.CaptureCallback _captureCallback;
   private WebShot.DocumentCallback _documentCallback;
   private WebShot.StatusCallback _statusCallback;
   public bool IsDocumentCallbackHandled { get; set; }
   
   public WebShot()
   {
      IsDocumentCallbackHandled = false;
      IntPtr ptr = IntPtr.Zero;
      int res = WebShot.OleInitialize(IntPtr.Zero);
      if (res != 0 && res != 1)
         throw new Exception(string.Format("WebShot.OleInitialize returned: {0}", res));
      _isOleInited = true;
      res = WebShot.Create(ref ptr);
      if (res == 0)
         throw new Exception("Can't create WebShotHandle");
      _isCreated = true;
      _ws = ptr;

      // Set the callback functions
      _captureCallback = new WebShot.CaptureCallback(OnCapture);
      if (WebShot.SetCaptureCallback(_ws, IntPtr.Zero, _captureCallback) == 0)
         throw new Exception(string.Format("SetCaptureCallback error: {0}", GetWebShotError()));
      _documentCallback =  new WebShot.DocumentCallback(OnDocument);
      if (WebShot.SetDocumentCallback(_ws, IntPtr.Zero, _documentCallback) == 0)
         throw new Exception(string.Format("SetDocumentCallback error: {0}", GetWebShotError()));
      //_statusCallback = new WebShot.StatusCallback(OnStatus);
      //if (WebShot.SetStatusCallback(_ws, IntPtr.Zero, _statusCallback) == 0)
      //   throw new Exception(string.Format("SetStatusCallback error: {0}", GetWebShotError()));
      //if (WebShot.SetExitCallback(_ws, IntPtr.Zero, new WebShot.ExitCallback(OnExit)) == 0)
      //   throw new Exception(string.Format("SetExitCallback error: {0}", GetWebShotError()));
   }
   public void Open(string url)
   {
      if (WebShot.Open(_ws, url) == 0)
         throw new Exception(string.Format("Open error: {0}", GetWebShotError()));
   }
   public void Dispose()
   {
      if (!_isDisposed)
      {
         if (_isCreated)
         {
            try { WebShot.Destroy(ref _ws); }
            catch { }
            _isCreated = false;
         }
         if (_isOleInited)
         {
            try { WebShot.OleUninitialize(); }
            catch { }
            _isOleInited = false;
         }
         _ws = IntPtr.Zero;
         _isDisposed = true;
         _captureCallback = null;
         _documentCallback = null;
      }
   }
   public int PageTimeout
   {
      get
      {
         int res = -1;
         if (WebShot.GetPageTimeout(_ws, ref res) == 0)
            throw new Exception(string.Format("GetPageTimeout failed: {0}", GetWebShotError()));
         return res;
      }
      set
      {
         if (WebShot.SetPageTimeout(_ws, value) == 0)
             throw new Exception(string.Format("SetPageTimeout error: {0}", GetWebShotError()));
      }
   }
   public int MetaRefreshTimeout
   {
      get
      {
         int res = -1;
         if (WebShot.GetMetaRefreshTimeout(_ws, ref res) == 0)
            throw new Exception(string.Format("GetMetaRefreshTimeout failed: {0}", GetWebShotError()));
         return res;
      }
      set
      {
         if (WebShot.SetMetaRefreshTimeout(_ws, value) == 0)
            throw new Exception(string.Format("GetMetaRefreshTimeout error: {0}", GetWebShotError()));
      }
   }
   public int DocumentWait
   {
      get
      {
         int res = -1;
         if (WebShot.GetDocumentWait(_ws, ref res) == 0)
            throw new Exception(string.Format("GetDocumentWait failed: {0}", GetWebShotError()));
         return res;
      }
      set
      {
         if (WebShot.SetDocumentWait(_ws, value) == 0)
            throw new Exception(string.Format("SetDocumentWait error: {0}", GetWebShotError()));
      }
   }
   public int DocumentWaitFlash
   {
      get
      {
         int res = -1;
         if (WebShot.GetDocumentWaitFlash(_ws, ref res) == 0)
            throw new Exception(string.Format("GetDocumentWaitFlash failed: {0}", GetWebShotError()));
         return res;
      }
      set
      {
         if (WebShot.SetDocumentWaitFlash(_ws, value) == 0)
            throw new Exception(string.Format("SetDocumentWaitFlash error: {0}", GetWebShotError()));
      }
   }
   public int ImageWait
   {
      get
      {
         int res = -1;
         if (WebShot.GetImageWait(_ws, ref res) == 0)
            throw new Exception(string.Format("GetImageWait failed: {0}", GetWebShotError()));
         return res;
      }
      set
      {
         if (WebShot.SetImageWait(_ws, value) == 0)
            throw new Exception(string.Format("SetImageWait error: {0}", GetWebShotError()));
      }
   }
   public int RedirectMaximum
   {
      get
      {
         int res = -1;
         if (WebShot.GetRedirectMaximum(_ws, ref res) == 0)
            throw new Exception(string.Format("GetRedirectMaximum failed: {0}", GetWebShotError()));
         return res;
      }
      set
      {
         if (WebShot.SetRedirectMaximum(_ws, value) == 0)
            throw new Exception(string.Format("SetRedirectMaximum error: {0}", GetWebShotError()));
      }
   }
   public string CustomHeaders
   {
      get
      {
         StringBuilder sb = new StringBuilder(10240, 10240);
         if (WebShot.GetCustomHeaders(_ws, sb, sb.MaxCapacity) == 0)
            throw new Exception(string.Format("GetCustomHeaders failed: {0}", GetWebShotError()));
         return sb.ToString();
      }
      set
      {
         if (WebShot.SetCustomHeaders(_ws, value) == 0)
            throw new Exception(string.Format("SetCustomHeaders error: {0}", GetWebShotError()));
      }
   }
   public string PostData
   {
      get
      {
         StringBuilder sb = new StringBuilder(10240, 10240);
         if (WebShot.GetPostData(_ws, sb, sb.MaxCapacity) == 0)
            throw new Exception(string.Format("GetPostData failed: {0}", GetWebShotError()));
         return sb.ToString();
      }
      set
      {
         if (WebShot.SetPostData(_ws, value) == 0)
            throw new Exception(string.Format("SetPostData error: {0}", GetWebShotError()));
      }
   }
   public int BrowserHeight
   {
      get
      {
         int res = -1;
         if (WebShot.GetBrowserHeight(_ws, ref res) == 0)
            throw new Exception(string.Format("GetBrowserHeight failed: {0}", GetWebShotError()));
         return res;
      }
      set
      {
         if (WebShot.SetBrowserHeight(_ws, value) == 0)
            throw new Exception(string.Format("SetBrowserHeight error: {0}", GetWebShotError()));
      }
   }
   public int HeightMaximum
   {
      get
      {
         int res = -1;
         if (WebShot.GetBrowserHeightMaximum(_ws, ref res) == 0)
            throw new Exception(string.Format("GetBrowserHeightMaximum failed: {0}", GetWebShotError()));
         return res;
      }
      set
      {
         if (WebShot.SetBrowserHeightMaximum(_ws, value) == 0)
            throw new Exception(string.Format("SetBrowserHeightMaximum error: {0}", GetWebShotError()));

      }
   }
   public bool Verbose
   {
      get
      {
         int res = -1;
         if (WebShot.GetVerbose(_ws, ref res) == 0)
            throw new Exception(string.Format("GetVerbose failed: {0}", GetWebShotError()));
         return Convert.ToBoolean(res);
      }
      set
      {
         int v = Convert.ToInt32(value);
         if (WebShot.SetVerbose(_ws, v) == 0)
            throw new Exception(string.Format("SetVerbose error: {0}", GetWebShotError()));
      }
   }
   public int BrowserWidth
   {
      get
      {
         int res = -1;
         if (WebShot.GetBrowserWidth(_ws, ref res) == 0)
            throw new Exception(string.Format("GetBrowserWidth failed: {0}", GetWebShotError()));
         return res;
      }
      set
      {
         if (WebShot.SetBrowserWidth(_ws, value) == 0)
            throw new Exception(string.Format("SetBrowserWidth error: {0}", GetWebShotError()));
      }
   }
   public string OutputPath
   {
      get
      {
         StringBuilder sb = new StringBuilder(10240, 10240);
         if (WebShot.GetOutputPath(_ws, sb, sb.MaxCapacity) == 0)
            throw new Exception(string.Format("GetOutputPath failed: {0}", GetWebShotError()));
         return sb.ToString();
      }
      set
      {
         if (WebShot.SetOutputPath(_ws, value) == 0)
            throw new Exception(string.Format("SetOutputPath error: {0}", GetWebShotError()));
      }
   }
   public string HtmlFilename
   {
      get
      {
         StringBuilder sb = new StringBuilder(10240, 10240);
         if (WebShot.GetHtmlFilename(_ws, sb, sb.MaxCapacity) == 0)
            throw new Exception(string.Format("GetHtmlFilename failed: {0}", GetWebShotError()));
         return sb.ToString();
      }
      set
      {
         if (WebShot.SetHtmlFilename(_ws, value) == 0)
            throw new Exception(string.Format("SetHtmlFilename error: {0}", GetWebShotError()));
      }
   }
   private string GetWebShotError()
   {
      StringBuilder sb = new StringBuilder(10240, 10240);
      WebShot.GetError(_ws, sb, sb.MaxCapacity);
      return sb.ToString();
   }
   public bool TryGetHttpCode(out int res)
   {
      res = 0;
      if (WebShot.GetHttpCode(_ws, ref res) == 0)
         return false;
      return true;
   }
   public bool TryGetRedirectCount(out int res)
   {
      res = -1;
      if (WebShot.GetRedirectCount(_ws, ref res) == 0)
         return false;
      return true;
   }
   public bool TryGetUrl(ref string url)
   {
      url = string.Empty;
      StringBuilder result = new StringBuilder(10240, 10240);
      if (WebShot.GetUrl(_ws, result, result.MaxCapacity) == 0)
         return false;
      url = result.ToString();
      return true;
   }
   public bool TryGetRedirectUrl(ref string url)
   {
      url = string.Empty;
      StringBuilder result = new StringBuilder(10240, 10240);
      if (WebShot.GetRedirectUrl(_ws, result, result.MaxCapacity) == 0)
         return false;
      url = result.ToString();
      return true;
   }
   private int OnCapture(IntPtr ws, IntPtr userHandle, IntPtr hBitmap, ref int saveToFile)
   {
      if (CaptureEvent != null)
      {
         CaptureEvent(this, hBitmap, ref saveToFile);
      }
      return 1;
   }
   private int OnDocument(IntPtr ws, IntPtr userHandle, IHTMLDocument2 doc)
   {
      lock (this)
      {
         if (DocumentEvent != null && !IsDocumentCallbackHandled)
         {
            DocumentEvent(this, doc);
            IsDocumentCallbackHandled = true;
         }
      }
      return 1;
   }
   private int OnStatus(IntPtr ws, IntPtr userHandle, string status)
   {
      if (StatusEvent != null)
      {
         StatusEvent(this, status);
      }
      return 1;
   }
   //private int OnExit(IntPtr userHandle)
   //{
   //   if (ExitEvent != null)
   //   {
   //      ExitEvent(this, userHandle);
   //   }
   //   return 1;
   //}
}