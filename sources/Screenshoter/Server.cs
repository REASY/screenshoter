﻿using log4net;
using log4net.Config;
using log4net.Repository.Hierarchy;
using mshtml;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Screenshoter
{
   public delegate void ServerErrorDelegate(object sender, Exception ex);
   // State object for reading client data asynchronously
   public class StateObject
   {
      public List<byte[]> AllData = new List<byte[]>();
      // Client  socket.
      public Socket workSocket = null;
      // Size of receive buffer.
      public const int BufferSize = 1024;
      // Receive buffer.
      public byte[] buffer = new byte[BufferSize];
      // Received data string.
      public StringBuilder sb = new StringBuilder();

      internal void AddData(byte[] p)
      {
         lock (AllData)
            AllData.Add(p);
      }
      internal byte[] GetDataAndClear()
      {
         byte[] data = new byte[0];
         int size = 0;
         foreach (var item in AllData)
            size += item.Length;
         data = new byte[size];
         int i = 0;
         foreach (var item in AllData)
         {
            item.CopyTo(data, i);
            i += item.Length;
         }
         AllData.Clear();
         return data;
      }
   }
   public class WebShotStateObject
   {
      public JObject Config { get; private set; }
      public JObject Metadata { get; private set; }

      public WebShotStateObject(JObject cfg, JObject metadata)
      {
         Config = cfg;
         Metadata = metadata;
      }
   }
   /// <summary>
   /// Size of the screenshot
   /// </summary>
   public enum ScreenshotSizeEnum
   {
      /// <summary>
      /// Visible browser size
      /// </summary>
      Screen,
      /// <summary>
      /// Full page
      /// </summary>
      Page
   }
   public class Server : IDisposable
   {
      private readonly Dictionary<WebShot, WebShotStateObject> _wsHandle2JObject = new Dictionary<WebShot, WebShotStateObject>();
      // Thread signal.
      private readonly ManualResetEvent _allDone = new ManualResetEvent(false);
      private readonly ILog _logger = LogManager.GetLogger(typeof(Server));
      private readonly Settings _setting;
      private readonly List<Thread> _runningThreads = new List<Thread>();
      private Thread _tcpListenerThread;
      private volatile bool _disposed = false;
      private volatile bool _stop = false;
      private byte[] _parseErrorResponse = new byte[0];
      private byte[] _commandMissingResponse = new byte[0];
      private byte[] _unknowCommandResponse = new byte[0];
      private byte[] _pongResponse = new byte[0];
      private byte[] _parseOkResponse = new byte[0];
      public Server(Settings setting, bool isService = false)
      {
         if (setting == null)
            throw new ArgumentNullException("setting");
         _setting = setting;
         WebShot.IsService = isService;
         Initialize();
      }
      public void Start()
      {
         // TcpListener thread
         _tcpListenerThread = new Thread(new ParameterizedThreadStart(ThreadTcpListenerMethod));
         _tcpListenerThread.Name = "TcpListenerThread";
         _tcpListenerThread.IsBackground = true;
         try
         {
            _tcpListenerThread.Start();
         }
         catch (Exception ex)
         {
            string msg = string.Format("Start faild: {0}", ex.Message);
            _logger.Fatal(msg, ex);
            throw new Exception(msg, ex);
         }
         _logger.InfoFormat("Server started successfully. ThreadId: {0}", _tcpListenerThread.ManagedThreadId);
      }

      private void InitializeResponses()
      {
         JObject errorResponse = new JObject
         {
            { "response", "parse error"}
         };
         _parseErrorResponse = System.Text.Encoding.ASCII.GetBytes(errorResponse.ToString().Replace("\r\n", "") + "\n");
         JObject commandMissingResponse = new JObject
         {
            { "response", "command missing"}
         };
         _commandMissingResponse = System.Text.Encoding.ASCII.GetBytes(commandMissingResponse.ToString().Replace("\r\n", "") + "\n");
         JObject unknownCommandResponse = new JObject
         {
            { "response", "unknow command"}
         };
         _unknowCommandResponse = System.Text.Encoding.ASCII.GetBytes(unknownCommandResponse.ToString().Replace("\r\n", "") + "\n");
         JObject pongResponse = new JObject()
         {
             { "response" , "pong"}
         };
         _pongResponse = System.Text.Encoding.ASCII.GetBytes(pongResponse.ToString().Replace("\r\n", "") + "\n");
         JObject parseOkResponse = new JObject()
         {
             { "response" , "ok"}
         };
         _parseOkResponse = System.Text.Encoding.ASCII.GetBytes(parseOkResponse.ToString().Replace("\r\n", "") + "\n");
      }
      public void Stop()
      {
         try
         {
            Dispose();
         }
         catch (Exception ex)
         {
            throw new Exception(string.Format("An error occurred while try stop server: {0}", ex.Message), ex);
         }
         _logger.InfoFormat("Server stopped.", _tcpListenerThread.ManagedThreadId);
      }
      private void Initialize()
      {
         _logger.InfoFormat("Starting initialize with config: {0}", _setting.ToString());
         FileInfo fi = null;
         bool hasError = false;
         try
         {
            using (var fsStream = File.OpenRead(_setting.LoggerCfgPath)) { }
         }
         catch (Exception ex)
         {
            hasError = true;
            BasicConfigurator.Configure();
            _logger.Warn(string.Format("An error occured while try open Log4Net config file[{0}]: {1}. Will use basic configuration!", _setting.LoggerCfgPath, ex.Message), ex);
         }
         if (!hasError)
         {
            hasError = false;
            try { fi = new FileInfo(_setting.LoggerCfgPath); }
            catch (Exception ex)
            {
               BasicConfigurator.Configure();
               _logger.Warn(string.Format("An error occured while try to get FileInfo for config file[{0}]: {1}. Will use basic configuration!", _setting.LoggerCfgPath, ex.Message), ex);
               hasError = true;
            }
            if (!hasError)
               XmlConfigurator.ConfigureAndWatch(fi);
         }
         WebShot.DllInit("debug.log", WebShot.DEBUG_FLAGWINDOW | WebShot.DEBUG_FLAGFILE);
         InitializeResponses();
      }
      private void ThreadTcpListenerMethod(object obj)
      {
         int port = _setting.Port;
         _logger.InfoFormat("Tcp listener thread started at port: {0}", port);
         Socket listener = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
         IPEndPoint bindEndpoint = new IPEndPoint(IPAddress.Any, port);
         try
         {
            listener.Bind(bindEndpoint);
            listener.Listen(100);
         }
         catch (Exception ex)
         {
            string msg = string.Format("Can't bind listener: {0}", ex.Message);
            var exc = new Exception(msg, ex);
            throw exc;
         }
         while (!_stop)
         {
            // Set the event to nonsignaled state.
            _allDone.Reset();
            _logger.Info("Waiting for client...");
            try
            {
               listener.BeginAccept(new AsyncCallback(AcceptCallback), listener);
            }
            catch (Exception ex)
            {
               _logger.Warn(string.Format("BeginAccept error: {0}", ex.Message), ex);
               continue;
            }
            // Wait until a connection is made before continuing.
            _allDone.WaitOne();
         }
      }
      public void AcceptCallback(IAsyncResult ar)
      {
         // Signal the main thread to continue.
         _allDone.Set();
         try
         {
            // Get the socket that handles the client request.
            Socket listener = (Socket)ar.AsyncState;
            Socket handler = listener.EndAccept(ar);
            // Create the state object.
            StateObject state = new StateObject();
            state.workSocket = handler;
            handler.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0,
               new AsyncCallback(ReadCallback), state);
         }
         catch (Exception ex)
         {
            _logger.Warn(string.Format("Can't AcceptCallback: {0}.", ex.Message), ex);
         }
      }
      public void ReadCallback(IAsyncResult ar)
      {
         try
         {
            // Retrieve the state object and the handler socket
            // from the asynchronous state object.
            StateObject state = (StateObject)ar.AsyncState;
            Socket handler = state.workSocket;
            // Read data from the client socket. 
            int bytesRead = handler.EndReceive(ar);
            if (bytesRead > 0)
            {
               // Try find newline, because commands are sent in JSON format, 1 command per line.
               var buffer = state.buffer.Take(bytesRead).ToArray();
               var prevData = state.GetDataAndClear();
               var allCmd = new byte[prevData.Length + buffer.Length];
               prevData.CopyTo(allCmd, 0);
               buffer.CopyTo(allCmd, prevData.Length);
               var cmdsStr = GetCmds(allCmd);
               if (cmdsStr != null && cmdsStr.Count() > 0)
               {
                  int idx = GetLastNewLineIndex(buffer);
                  if (idx > 0)
                  {
                     if (idx == buffer.Length - 1)
                        buffer = null;
                     else
                     {
                        int newCnt = buffer.Length - idx - 1;
                        buffer = buffer.Skip(idx + 1).Take(newCnt).ToArray();
                     }
                  }
                  JObject json;
                  _logger.InfoFormat("Received command '{0}' from '{1}'", cmdsStr.First(), handler.RemoteEndPoint); 
                  if (!TryParseToJson(cmdsStr.First(), handler, out json))
                  {
                     SendResponse(handler, _parseErrorResponse);
                     CloseSocket(handler);
                     return;
                  }
                  object[] param = new object[] { json, handler };
                  //var task = Task.Factory.StartNew(HandlingRequest, param);
                  HandlingRequest(param);
                  CloseSocket(handler);
                  return;
               }
               if (buffer != null)
                     state.AddData(buffer);
               handler.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0,
                  new AsyncCallback(ReadCallback), state);
            }
            else
            {
               // All of the data has been read, so handling command
               byte[] request = state.GetDataAndClear();
               var cmdsStr = GetCmds(request);
               if (cmdsStr != null && cmdsStr.Count() > 0)
               {
                  JObject json;
                  _logger.InfoFormat("Received command '{0}' from '{1}'", cmdsStr.First(), handler.RemoteEndPoint);
                  if (!TryParseToJson(cmdsStr.First(), handler, out json))
                  {
                     SendResponse(handler, _parseErrorResponse);
                     CloseSocket(handler);
                     return;
                  }
                  object[] param = new object[] { json, handler };
                  //var task = Task.Factory.StartNew(HandlingRequest, param);
                  HandlingRequest(param);
                  CloseSocket(handler);
               }
            }
         }
         catch (Exception ex)
         {
            // TODO maybe we can proccess command? Will realize it next time
            _logger.Warn(string.Format("An error occured: {0}.", ex.Message), ex);
         }
      }

      private bool TryParseToJson(string cmd, Socket client, out JObject json)
      {
         json = null;
         try { json = JObject.Parse(cmd); }
         catch (JsonException jex)
         {
            _logger.Warn(string.Format("Can't parse JSON['{0}'] from '{1}': {2}", cmd, client.RemoteEndPoint, jex.Message), jex);
            return false;
         }
         return true;
      }
      private static string[] GetCmds(byte[] allCmd)
      {
         var requstStr = System.Text.Encoding.ASCII.GetString(allCmd);
         var cmdsStr = requstStr.Split(new string[] { "\r\n" }, StringSplitOptions.None);
         return cmdsStr;
      }

      private int GetLastNewLineIndex(byte[] buffer)
      {
         int length = buffer.Length;
         for (int i = length-1; i >= 0; i--)
         {
            if (buffer[i] == '\n' && i != 0 && buffer[i - 1] == '\r')
               return i;
         }
         return -1;
      }
      /// <summary>
      /// <para>Handling requist</para>
      /// </summary>
      /// <param name="data">Data</param>
      /// <param name="client">Client socket</param>
      private void HandlingRequest(object obj)
      {
         object[] param = (object[])obj;
         JObject json = (JObject)param[0];
         Socket client = (Socket)param[1];
         try
         {
            JToken token;
            if (!json.TryGetValue("command", out token))
            {
               HandlingCommandMiss(json, client);
               return;
            }
            var cmd = token.Value<string>();
            cmd = cmd.ToLowerInvariant();
            switch (cmd)
            {
               case "capture":
                  HandlingCapture(json, client);
                  SendResponse(client, _parseOkResponse);
                  break;
               case "info":
                  HandlingInfo(json, client);
                  break;
               case "ping":
                  HandlingPing(json, client);
                  break;
               default:
                  HandlingUnknowCommand(json, client);
                  break;
            }
         }
         catch (Exception ex)
         { }
         //finally
         //{
         //   try { CloseSocket(client); }
         //   catch{}
         //}
      }

      private void CloseSocket(Socket client)
      {
         _logger.InfoFormat("Close connection with '{0}'", client.RemoteEndPoint);
         if (client.Connected)
         {
            try { client.Shutdown(SocketShutdown.Both); }
            catch { }
         }
         try { client.Close(); }
         catch { }
      }

      private void HandlingPing(JObject json, Socket client)
      {
         _logger.Info("HandlingPing");         
         SendResponse(client, _pongResponse);
         _logger.Info("Sent Ping response");
      }

      private static void SendResponse(Socket client, byte[] bytes)
      {
         client.Send(bytes);
      }

      private void HandlingCommandMiss(JObject json, Socket client)
      {
         _logger.Info("HandlingCommandMiss");
         SendResponse(client, _commandMissingResponse);
         _logger.Info("Sent CommandMissing response");
      }
      private void HandlingInfo(JObject json, Socket client)
      {
         _logger.Info("HandlingInfo");
         int runCnt = 0;
         lock(_runningThreads)
            runCnt = _runningThreads.Count;
         JObject response = new JObject()
         {
             { "capacity" , _setting.Capacity},
             { "count", runCnt},
             { "available", _setting.Capacity -runCnt }
         };
         // TODO Will use Asyn next time
         var str = response.ToString().Replace("\r\n", "");
         var bytes = System.Text.Encoding.ASCII.GetBytes(str + "\n");
         SendResponse(client, bytes);
         _logger.Info("Sent Info response");
      }

      private void HandlingUnknowCommand(JObject json, Socket client)
      {
         _logger.Info("HandlingUnknowCommand");
         SendResponse(client, _unknowCommandResponse);
         _logger.Info("Sent UnknowCommand response");
      }

      private void HandlingCapture(JObject json, Socket client)
      {
         _logger.Info("HandlingCapture");
         object[] p = new object[] { json, client };
         try
         {
            Thread thread = new Thread(new ParameterizedThreadStart(ThreadHandlingCapture));
            thread.Name = "ThreadHandlingCapture_" + Guid.NewGuid().ToString();
            thread.SetApartmentState(System.Threading.ApartmentState.STA);
            thread.Start(p);
            lock (_runningThreads)
               _runningThreads.Add(thread);
         }
         catch (Exception ex)
         {
            throw new Exception(string.Format("Can't start new thread for capture: {0}", ex.Message), ex);
         }
      }
      private void ThreadHandlingCapture(object obj)
      {
         object[] objs = (object[])obj;
         JObject cfg = (JObject)objs[0];
         Socket client = (Socket)objs[1];
         try
         {
            CaptureScreenshot(cfg.DeepClone() as JObject);
         }
         catch (Exception ex)
         {
            _logger.Warn(string.Format("Error while try capture screenshot: {0}", ex.Message), ex);
         }
         lock (_runningThreads)
            _runningThreads.Remove(Thread.CurrentThread);
      }
      private void CaptureScreenshot(JObject cfg)
      {
         int runCnt = 0;
         lock (_runningThreads)
            runCnt = _runningThreads.Count;
         if (runCnt > _setting.Capacity)
         {
            _logger.WarnFormat("Capacity: {0}, Running screenshoters: {1}", _setting.Capacity, runCnt);
         }
         WebShot ws = null;
         try
         {
            ws = new WebShot();
            JObject metadata = cfg.DeepClone() as JObject;
            WebShotStateObject state = new WebShotStateObject(cfg, metadata);
            lock(_wsHandle2JObject)
               _wsHandle2JObject[ws] = state;
            try
            {
               ConfigurateWebShot(cfg, ws);
               ws.CaptureEvent += ws_CaptureEvent;
               ws.DocumentEvent += ws_DocumentEvent;
               metadata["width"] = metadata["screen_width"];
               metadata["height"] = metadata["screen_height"];
               metadata["started"] = GetTimestamp();
               metadata["loaded"] = 0;
               string url = cfg["url"].Value<string>();
               _logger.InfoFormat("Opening url: '{0}", url);
               ws.Open(url);
               string u = string.Empty;
               ws.TryGetRedirectUrl(ref u);
               _logger.Info("Screenshot done"); 
            }
            finally
            {
               Exception ex = null;
               string path = state.Config["file"].Value<string>() + ".finished";
               try
               {
                  WriteMetadata(path, state.Metadata);
                  _logger.InfoFormat("Metadata wrote to file '{0}'", path);
               }
               catch (Exception exp)
               {
                  ex = new Exception(string.Format("An error occurred while try write metadata: {0}", exp.Message), exp);
               }
               lock(_wsHandle2JObject)
                  _wsHandle2JObject.Remove(ws);
               if (ex != null)
                  throw ex;
            }
         }
         finally
         {
            if (ws != null)
            {
               try { ws.Dispose(); }
               catch(Exception ex)
               {
                  _logger.ErrorFormat("Dispose() Exception invoked in dispose: {0}", ex.Message);
               }
               ws.CaptureEvent -= ws_CaptureEvent;
               ws.DocumentEvent -= ws_DocumentEvent;
            }
         }
      }

      void ws_DocumentEvent(object sender, IHTMLDocument2 doc)
      {
         _logger.InfoFormat("DocumentEvent. Doc.readyState : {0}. Final URL: {1}, Parent: {2}", doc.readyState, doc.url, doc.parentWindow);
         WebShot ws = sender as WebShot;
         try
         {
            WebShotStateObject state = _wsHandle2JObject[ws];
            JObject cfg = state.Config;
            JObject jo = state.Metadata;
            int temp = -1;
            // TODO On www.yandex.ru I can't get redirections...            
            if (ws.TryGetRedirectCount(out temp))
            {
               jo["redirections"] = temp;
               
            }
            else
               jo["redirections"] = 0;
            jo["final_url"] = doc.url;
            jo["loaded"] = 1;
            jo["content"] = 1;
            jo["content_time"] = GetTimestamp();
            // TODO On www.yandex.ru I can't get http code...
            if (ws.TryGetHttpCode(out temp))
               jo["response_code"] = temp;
            else
               jo["response_code"] = -1;
            jo["content_type"] = doc.mimeType;
            JToken token;
            if (cfg.TryGetValue("script", out token))
            {
               string script = token.Value<string>();
               if (!string.IsNullOrEmpty(script))
                  doc.parentWindow.execScript(script, "JScript");
            }
            if (cfg.TryGetValue("details", out token))
            {
               int details = token.Value<int>();
               if (details == 3)
               {
                  string[] source = new string[doc.images.length];
                  int i = 0;
                  // images
                  foreach (var item in doc.images)
                  {
                     var src = ((dynamic)item).src;
                     if (src != null)
                        source[i++] = src;
                  }
                  jo["images"] = new JArray(source.Take(i));
                  // scripts
                  source = new string[doc.scripts.length];
                  i = 0;
                  foreach (var item in doc.scripts)
                  {
                     var src = ((dynamic)item).src;
                     if (src != null)
                        source[i++] = src;
                  }
                  jo["scripts"] = new JArray(source.Take(i));
                  // stylesheets
                  source = new string[doc.scripts.length];
                  i = 0;
                  foreach (var item in doc.styleSheets)
                  {
                     var href = ((dynamic)item).href;
                     if (href != null)
                        source[i++] = href;
                  }
                  jo["stylesheets"] = new JArray(source.Take(i));
                  // embeds
                  source = new string[doc.embeds.length];
                  i = 0;
                  foreach (var item in doc.embeds)
                  {
                     var src = ((dynamic)item).src;
                     if (src != null)
                        source[i++] = src;
                  }
                  jo["embeds"] = new JArray(source.Take(i));
                  // applets
                  source = new string[doc.applets.length];
                  i = 0;
                  foreach (var item in doc.applets)
                  {
                     var codebase = ((dynamic)item).codeBase;
                     if (codebase != null)
                        source[i++] = codebase;
                  }
                  jo["applets"] = new JArray(source.Take(i));
                  // iframes
                  source = new string[doc.frames.length];
                  i = 0;
                  for (int k = 0; k < doc.frames.length; k++)
                  {
                     object refIndex = i;
                     var frame = doc.frames.item(ref refIndex);
                     var frmSrc = frame.frameElement.src;
                     if (frmSrc != null)
                        source[i++] = frmSrc;
                  }
                  jo["iframes"] = new JArray(source.Take(i));
               }
            }
         }
         catch (Exception ex)
         {
            _logger.Warn(string.Format("An error occured: {0}", ex.Message), ex);
         }
      }

      void ws_CaptureEvent(object sender, IntPtr hBitmap, ref int saveToFile)
      {
         WebShot ws = sender as WebShot;
         try
         {
            WebShotStateObject state = _wsHandle2JObject[ws];
            JObject cfg = state.Config;
            JObject jo = state.Metadata;
            //string url = string.Empty;
            //if (ws.TryGetUrl(ref url))
            //   jo["final_url"] = url;
            //string url = string.Empty;;
            //if (ws.TryGetRedirectUrl(ref url))
            //   jo["final_url"] = url;
            jo["status"] = "OK";
            jo["error"] = 0;
            jo["finished"] = GetTimestamp();
            // If details == 3 handled in ws_DocumentEvent, because there I have no access to IHTMLDocument
            //JToken token;
            //if (cfg.TryGetValue("details", out token))
            //{
            //   int details = token.Value<int>();
            //   if (details == 3)
            //   {
            //   }
            //}
         }
         catch (Exception ex)
         {
            _logger.Warn(string.Format("An error occured: {0}", ex.Message), ex);
         }
      }

      private void WriteMetadata(string path, JObject jObject)
      {
         using (StreamWriter file = File.CreateText(path))
         using (JsonTextWriter writer = new JsonTextWriter(file))
         {
            jObject.WriteTo(writer);
         }
      }

      //private int DocumentCallback(IntPtr ws, IntPtr userHandle, IHTMLDocument2 doc)
      //{
      //   _logger.InfoFormat("DocumentCallback. Doc.readyState : {0}", doc.readyState);
      //   try
      //   {
      //      WebShotStateObject state = _wsHandle2JObject[ws];
      //      JObject cfg = state.Config;
      //      JObject jo = state.Metadata;
      //      int temp = -1;
      //      // TODO On www.yandex.ru I can't get redirections...
      //      if (WebShot.GetRedirectCount(ws, ref temp) != 0)
      //         jo["redirections"] = temp;
      //      else
      //         jo["redirections"] = -1;
      //      jo["loaded"] = 1;
      //      jo["content"] = 1;
      //      jo["content_time"] = GetTimestamp();
      //      // TODO On www.yandex.ru I can't get http code...
      //      if (WebShot.GetHttpCode(ws, ref temp) != 0)
      //         jo["response_code"] = temp;
      //      else
      //         jo["response_code"] = -1;
      //      jo["content_type"] = doc.mimeType;
      //      JToken token;
      //      if (cfg.TryGetValue("script", out token))
      //      {
      //         string script = token.Value<string>();
      //         if (!string.IsNullOrEmpty(script))
      //            doc.parentWindow.execScript(script, "JScript");
      //      }
      //      if (cfg.TryGetValue("details", out token))
      //      {
      //         int details = token.Value<int>();
      //         if (details == 3)
      //         {
      //            string[] source = new string[doc.images.length];
      //            int i = 0;
      //            // images
      //            foreach (var item in doc.images)
      //            {
      //               var src = ((dynamic)item).src;
      //               if (src != null)
      //                  source[i++] = src;
      //            }
      //            jo["images"] = new JArray(source.Take(i));
      //            // scripts
      //            source = new string[doc.scripts.length];
      //            i = 0;
      //            foreach (var item in doc.scripts)
      //            {
      //               var src = ((dynamic)item).src;
      //               if (src != null)
      //                  source[i++] = src;
      //            }
      //            jo["scripts"] = new JArray(source.Take(i));
      //            // stylesheets
      //            source = new string[doc.scripts.length];
      //            i = 0;
      //            foreach (var item in doc.styleSheets)
      //            {
      //               var href = ((dynamic)item).href;
      //               if (href != null)
      //                  source[i++] = href;
      //            }
      //            jo["stylesheets"] = new JArray(source.Take(i));
      //            // embeds
      //            source = new string[doc.embeds.length];
      //            i = 0;
      //            foreach (var item in doc.embeds)
      //            {
      //               var src = ((dynamic)item).src;
      //               if (src != null)
      //                  source[i++] = src;
      //            }
      //            jo["embeds"] = new JArray(source.Take(i));
      //            // applets
      //            source = new string[doc.applets.length];
      //            i = 0;
      //            foreach (var item in doc.applets)
      //            {
      //               var codebase = ((dynamic)item).codeBase;
      //               if (codebase != null)
      //                  source[i++] = codebase;
      //            }
      //            jo["applets"] = new JArray(source.Take(i));
      //            // iframes
      //            source = new string[doc.frames.length];
      //            i = 0;
      //            for (int k = 0; k < doc.frames.length; k++)
      //            {
      //               object refIndex = i;
      //               var frame = doc.frames.item(ref refIndex);
      //               var frmSrc = frame.frameElement.src;
      //               if (frmSrc != null)
      //                  source[i++] = frmSrc;
      //            }
      //            jo["iframes"] = new JArray(source.Take(i));
      //         }
      //      }
      //   }
      //   catch(Exception ex)
      //   {
      //      _logger.Warn(string.Format("An error occured: {0}", ex.Message), ex);
      //   }
      //   return 1;
      //}
      //private int CaptureCallback(IntPtr ws, IntPtr userHandle, IntPtr hBitmap, ref int saveToFile)
      //{
      //   try
      //   {
      //      WebShotStateObject state = this._wsHandle2JObject[ws];
      //      JObject cfg = state.Config;
      //      JObject jo = state.Metadata;
      //      StringBuilder url = new StringBuilder();
      //      if (WebShot.GetUrl(ws, url, 2000) != 0)
      //         jo["final_url"] = url.ToString();
      //      jo["final_url"] = "### EMPTY ###";
      //      jo["status"] = "OK";
      //      jo["error"] = 0;
      //      jo["finished"] = GetTimestamp();
      //      JToken token;
      //      if (cfg.TryGetValue("details", out token))
      //      {
      //         int details = token.Value<int>();
      //         if (details == 3)
      //         {
      //         }
      //      }
      //   }
      //   catch (Exception ex)
      //   {
      //      _logger.Warn(string.Format("An error occured: {0}", ex.Message), ex);
      //   }
      //   return 1;
      //}
      private long GetTimestamp()
      {
         return DateTime.Now.ToFileTimeUtc();
      }
      public void ConfigurateWebShot(JObject json, WebShot ws)
      {
         //WebShot.SetVerbose(ws, 1);
         ws.PageTimeout = _setting.Timeout;
         ws.MetaRefreshTimeout = _setting.Timeout;
         ws.DocumentWait = json["delay"].Value<int>();
         ws.DocumentWaitFlash = json["flash_delay"].Value<int>();
         ws.ImageWait = json["delay"].Value<int>();
         ws.RedirectMaximum = _setting.Redirections;
         string customHeaders = string.Empty;
         string cookie = json["cookie"].Value<string>();
         if (!string.IsNullOrEmpty(cookie))
            customHeaders = "Cookie: " + cookie;
         string referer = json["referer"].Value<string>();
         if (!string.IsNullOrEmpty(referer))
         {
            if (string.IsNullOrEmpty(customHeaders))
               customHeaders = "Referer: " + referer;
            else
               customHeaders += "\r\nReferer: " + referer;
         }
         if (!string.IsNullOrEmpty(customHeaders))
         {
            ws.CustomHeaders = customHeaders;
         }
         string postData = json["post_data"].Value<string>();
         if (!string.IsNullOrEmpty(customHeaders))
         {
            ws.PostData = postData;
         }
         ws.BrowserHeight = json["screen_height"].Value<int>();
         ws.BrowserWidth = json["screen_width"].Value<int>();        
         // Set the output
         bool isHtml = json["html"].Value<int>() == 0 ? false : true;
         //json["file"] = Path.GetFileNameWithoutExtension(path) + "__" + Guid.NewGuid().ToString() + "__" + Path.GetExtension(path);
         string pngFile = json["file"].Value<string>();
         string htmlFile = pngFile + ".html";
         //pngFile = Path.GetDirectoryName(pngFile) + "\\" + Path.GetFileNameWithoutExtension(pngFile) + "__" + Guid.NewGuid().ToString() + "__" + Path.GetExtension(pngFile);
         //htmlFile = Path.GetDirectoryName(htmlFile) + "\\" + Path.GetFileNameWithoutExtension(htmlFile) + "__" + Guid.NewGuid().ToString() + "__" + Path.GetExtension(htmlFile);
         ws.OutputPath = pngFile;         
         // Set html output if need
         if (isHtml)
            ws.HtmlFilename = htmlFile;
         // Size of the screenshot
         ScreenshotSizeEnum sse = GetScreenshotSize(json["size"].Value<string>());
         if (sse == ScreenshotSizeEnum.Page)
         {
            ws.HeightMaximum = 10000;
         }
      }

      private ScreenshotSizeEnum GetScreenshotSize(string val)
      {
         if (val == "page")
            return ScreenshotSizeEnum.Page;
         else return ScreenshotSizeEnum.Screen;
      }
      //private void ConfigurateWebshot(JObject json, IntPtr ws)
      //{
      //   WebShot.SetVerbose(ws, 1);
      //   bool hasWarn = false;
      //   _logger.InfoFormat("Configurating WebShot with: {0}", json.ToString());
      //   if (WebShot.SetPageTimeout(ws, this._setting.Timeout) == 0)
      //   {
      //      this._logger.WarnFormat("SetPageTimeout error: {0}", this.GetWebshotError(ws));
      //      hasWarn = true;
      //   }
      //   if (WebShot.SetMetaRefreshTimeout(ws, this._setting.Timeout) == 0)
      //   {
      //      this._logger.WarnFormat("SetMetaRefreshTimeout error: {0}", this.GetWebshotError(ws));
      //      hasWarn = true;
      //   }
      //   if (WebShot.SetDocumentWait(ws, json["delay"].Value<int>()) == 0)
      //   {
      //      this._logger.WarnFormat("SetDocumentWait error: {0}", this.GetWebshotError(ws));
      //      hasWarn = true;
      //   }
      //   if (WebShot.SetDocumentWaitFlash(ws, json["flash_delay"].Value<int>()) == 0)
      //   {
      //      this._logger.WarnFormat("SetDocumentWaitFlash error: {0}", this.GetWebshotError(ws));
      //      hasWarn = true;
      //   }
      //   if (WebShot.SetImageWait(ws, json["delay"].Value<int>()) == 0)
      //   {
      //      this._logger.WarnFormat("SetImageWait error: {0}", this.GetWebshotError(ws));
      //      hasWarn = true;
      //   }
      //   if (WebShot.SetRedirectMaximum(ws, this._setting.Redirections) == 0)
      //   {
      //      this._logger.WarnFormat("SetRedirectMaximum error: {0}", this.GetWebshotError(ws));
      //      hasWarn = true;
      //   }
      //   string customHeaders = string.Empty;
      //   string cookie = json["cookie"].Value<string>();
      //   if (!string.IsNullOrEmpty(cookie))
      //      customHeaders = "Cookie: " + cookie;
      //   string referer = json["referer"].Value<string>();
      //   if (!string.IsNullOrEmpty(referer))
      //   {
      //      if (string.IsNullOrEmpty(customHeaders))
      //         customHeaders = "Referer: " + referer;
      //      else
      //         customHeaders += "\r\nReferer: " + referer;
      //   }
      //   if (!string.IsNullOrEmpty(customHeaders))
      //      if (WebShot.SetCustomHeaders(ws, customHeaders) == 0)
      //      {
      //         this._logger.WarnFormat("SetCustomHeaders error: {0}", this.GetWebshotError(ws));
      //         hasWarn = true;
      //      }
      //   if (WebShot.SetPostData(ws, json["post_data"].Value<string>()) == 0)
      //   {
      //      this._logger.WarnFormat("SetPostData error: {0}", this.GetWebshotError(ws));
      //      hasWarn = true;
      //   }
      //   if (WebShot.SetBrowserHeight(ws, json["screen_height"].Value<int>()) == 0)
      //   {
      //      this._logger.WarnFormat("SetBrowserHeight error: {0}", this.GetWebshotError(ws));
      //      hasWarn = true;
      //   }
      //   if (WebShot.SetBrowserWidth(ws, json["screen_width"].Value<int>()) == 0)
      //   {
      //      this._logger.WarnFormat("SetBrowserWidth error: {0}", this.GetWebshotError(ws));
      //      hasWarn = true;
      //   }
      //   var captureDeletate = new WebShot.CaptureCallback(this.CaptureCallback);
      //   _wsHandle2JObject[ws].CaptureDeletate = captureDeletate;
      //   if (WebShot.SetCaptureCallback(ws, IntPtr.Zero, captureDeletate) == 0)
      //   {
      //      this._logger.WarnFormat("SetCaptureCallback error: {0}", this.GetWebshotError(ws));
      //      hasWarn = true;
      //   }
      //   var documentDelegate = new WebShot.DocumentCallback(this.DocumentCallback);
      //   _wsHandle2JObject[ws].DocumentDelegate = documentDelegate;
      //   if (WebShot.SetDocumentCallback(ws, IntPtr.Zero, documentDelegate) == 0)
      //   {
      //      this._logger.WarnFormat("SetDocumentCallback error: {0}", this.GetWebshotError(ws));
      //      hasWarn = true;
      //   }
      //   string path = (json["html"].Value<int>() != 0) ? (json["file"].Value<string>() + ".html") : json["file"].Value<string>();
      //   path = Path.GetDirectoryName(path) + "\\" + Path.GetFileNameWithoutExtension(path) + "__" + Guid.NewGuid().ToString() + "__" + Path.GetExtension(path);
      //   if (WebShot.SetOutputPath(ws, path) == 0)
      //   {
      //      this._logger.WarnFormat("SetOutputPath error: {0}", this.GetWebshotError(ws));
      //      hasWarn = true;
      //   }
      //   if (hasWarn)
      //      _logger.Info("Configurated with warnings! See above for more detail.");
      //   else
      //      _logger.Info("Configurated success");
      //}
      private string GetWebshotError(IntPtr ws)
      {
         StringBuilder sb = new StringBuilder(10240, 10240);
         WebShot.GetError(ws, sb, sb.MaxCapacity);
         return sb.ToString();
      }
      int StatusCallback(IntPtr WebShotHandle, IntPtr UserHandle, string Status)
      {
         Console.WriteLine("Status: " + Status);
         return 1;
      }
      private byte[] GetBytes(List<byte[]> result)
      {
         byte[] data;
         int size = 0;
         foreach (var item in result)
            size += item.Length;
         data = new byte[size];
         int i = 0;
         foreach (var item in result)
         {
            item.CopyTo(data, i);
            i += item.Length;
         }
         return data;
      }

      public void Dispose()
      {
         if (!_disposed)
         {
            _allDone.Set();
            _stop = true;
            _disposed = true;
            WebShot.DllUninit();
         }
      }

      public void StartDebug()
      {
         string str = File.ReadAllText("CaptureCommand.json");
         JObject json = JObject.Parse(str);
         object[] p = new object[] { json, null };
         try
         {
            Thread thread = new Thread(new ParameterizedThreadStart(ThreadHandlingCapture));
            thread.Name = "ThreadHandlingCapture";
            thread.SetApartmentState(System.Threading.ApartmentState.STA);
            thread.Start(p);
            _runningThreads.Add(thread);
         }
         catch (Exception ex)
         {
            throw new Exception(string.Format("Can't start new thread for capture: {0}", ex.Message), ex);
         }
      }
      public void StartDebug2()
      {
         string str = File.ReadAllText("CaptureCommand.json");
         JObject json = JObject.Parse(str);
         object[] p = new object[] { json, null };
         try
         {
            ThreadHandlingCapture(p);
         }
         catch (Exception ex)
         {
            throw new Exception(string.Format("Can't start new thread for capture: {0}", ex.Message), ex);
         }
      }
   }
}
