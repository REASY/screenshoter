using System;
using System.Text;
using System.Runtime.InteropServices;
using mshtml; // Microsoft.mshtml primary Interop assembly

/*********************************************************************/

public partial class WebShot
{
   public enum TableValue
   {
      x32_nosvc = 0,
      x32_svc = 1,
      x64_nosvc = 2,
      x64_svc = 3
   }
   private static TableValue GetTable()
   {
      TableValue res = 0;
      if (Environment.Is64BitProcess)
      {
         if (IsService)
            res = TableValue.x64_svc;
         else
            res = TableValue.x64_nosvc;
      }
      else
      {
         if (IsService)
            res = TableValue.x32_svc;
         else
            res = TableValue.x32_nosvc;
      }
      return res;
   }


   public static bool IsService { get; set; }
   [DllImport("ole32.dll")]
   public static extern int OleInitialize(IntPtr pvReserved);
   [DllImport("ole32.dll")]
   public static extern void OleUninitialize();

   public const int DEBUG_FLAGDISABLED = (0x00000000);
   public const int DEBUG_FLAGWINDOW = (0x00000001);
   public const int DEBUG_FLAGFILE = (0x00000002);
   public const int DEBUG_FLAGVERBOSE = (0x00000004);
   public const int DEBUG_FLAGAPPEND = (0x00000008);
   public const int DEBUG_FLAGCOMMANDLINE = (0x00000010);
   public const int DEBUG_FLAGTIMESTAMP = (0x00000020);
   public const int DEBUG_FLAGPERTHREAD = (0x00000100);
   public const int DEBUG_FLAGNOOUTPUT = (0x00010000);

   /*********************************************************************/

   public delegate int ProgressCallback(IntPtr WebShotHandle, IntPtr UserHandle, float Progress);
   public delegate int StatusCallback(IntPtr WebShotHandle, IntPtr UserHandle, [MarshalAs(UnmanagedType.LPWStr)] string Status);
   public delegate int DocumentCallback(IntPtr WebShotHandle, IntPtr UserHandle, [In, MarshalAs(UnmanagedType.Interface)] IHTMLDocument2 Document);
   public delegate int CaptureCallback(IntPtr WebShotHandle, IntPtr UserHandle, IntPtr hBitmap, ref int SaveToFile);
   public delegate int ExitCallback(IntPtr UserHandle);

   /*********************************************************************/

   [DllImport("webshot64.dll", EntryPoint = "WebShot_Stop", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int Stop_64(IntPtr WebShotHandle);
   [DllImport("webshot.dll", EntryPoint = "WebShot_Stop", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int Stop_32(IntPtr WebShotHandle);
   [DllImport("webshot_svc64.dll", EntryPoint = "WebShot_Stop", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
   private static extern int Stop_svc64(IntPtr WebShotHandle);
   [DllImport("webshot_svc.dll", EntryPoint = "WebShot_Stop", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
   private static extern int Stop_svc32(IntPtr WebShotHandle);
   public static int Stop(IntPtr ws)
   {
      var t = GetTable();
      switch (t)
      {
         case TableValue.x32_nosvc:
            return Stop_32(ws);
         case TableValue.x32_svc:
            return Stop_svc32(ws);
         case TableValue.x64_nosvc:
            return Stop_64(ws);
         case TableValue.x64_svc:
            return Stop_svc64(ws);
      }
      return -1;
   }
   [DllImport("webshot64.dll", EntryPoint = "WebShot_Open", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int Open_64(IntPtr WebShotHandle, string Url);
   [DllImport("webshot.dll", EntryPoint = "WebShot_Open", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int Open_32(IntPtr WebShotHandle, string Url);
   [DllImport("webshot_svc64.dll", EntryPoint = "WebShot_Open", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
   private static extern int Open_svc64(IntPtr WebShotHandle, string Url);
   [DllImport("webshot_svc.dll", EntryPoint = "WebShot_Open", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
   private static extern int Open_svc32(IntPtr WebShotHandle, string Url);
   public static int Open(IntPtr ws, string url)
   {
      var t = GetTable();
      switch (t)
      {
         case TableValue.x32_nosvc:
            return Open_32(ws, url);
         case TableValue.x32_svc:
            return Open_svc32(ws, url);
         case TableValue.x64_nosvc:
            return Open_64(ws, url);
         case TableValue.x64_svc:
            return Open_svc64(ws, url);
      }
      return -1;
   }

   [DllImport("webshot64.dll", EntryPoint = "WebShot_Process", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int Process_64(IntPtr WebShotHandle);
   [DllImport("webshot.dll", EntryPoint = "WebShot_Process", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int Process_32(IntPtr WebShotHandle);
   public static int Process(IntPtr ws)
   {
      if (Environment.Is64BitProcess)
         return Process_64(ws);
      else
         return Process_32(ws);
   }

   [DllImport("webshot64.dll", EntryPoint = "WebShot_SetProgressCallback", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int SetProgressCallback_64(IntPtr WebShotHandle, IntPtr UserHandle, ProgressCallback Callback);
   [DllImport("webshot.dll", EntryPoint = "WebShot_SetProgressCallback", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int SetProgressCallback_32(IntPtr WebShotHandle, IntPtr UserHandle, ProgressCallback Callback);
   public static int SetProgressCallback(IntPtr WebShotHandle, IntPtr UserHandle, ProgressCallback Callback)
   {
      if (Environment.Is64BitProcess)
         return SetProgressCallback_64(WebShotHandle, UserHandle, Callback);
      else
         return SetProgressCallback_32(WebShotHandle, UserHandle, Callback);
   }

   [DllImport("webshot64.dll", EntryPoint = "WebShot_SetStatusCallback", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int SetStatusCallback_64(IntPtr WebShotHandle, IntPtr UserHandle, StatusCallback Callback);
   [DllImport("webshot.dll", EntryPoint = "WebShot_SetStatusCallback", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int SetStatusCallback_32(IntPtr WebShotHandle, IntPtr UserHandle, StatusCallback Callback);
   [DllImport("webshot_svc64.dll", EntryPoint = "WebShot_SetStatusCallback", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
   private static extern int SetStatusCallback_svc64(IntPtr WebShotHandle, IntPtr UserHandle, StatusCallback Callback);
   [DllImport("webshot_svc.dll", EntryPoint = "WebShot_SetStatusCallback", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
   private static extern int SetStatusCallback_svc32(IntPtr WebShotHandle, IntPtr UserHandle, StatusCallback Callback);
   public static int SetStatusCallback(IntPtr WebShotHandle, IntPtr UserHandle, StatusCallback Callback)
   {
      var t = GetTable();
      switch (t)
      {
         case TableValue.x32_nosvc:
            return SetStatusCallback_32(WebShotHandle, UserHandle, Callback);
         case TableValue.x32_svc:
            return SetStatusCallback_svc32(WebShotHandle, UserHandle, Callback);
         case TableValue.x64_nosvc:
            return SetStatusCallback_64(WebShotHandle, UserHandle, Callback);
         case TableValue.x64_svc:
            return SetStatusCallback_svc64(WebShotHandle, UserHandle, Callback);
      }
      if (Environment.Is64BitProcess)
         return SetStatusCallback_64(WebShotHandle, UserHandle, Callback);
      else
         return SetStatusCallback_32(WebShotHandle, UserHandle, Callback);
   }

   [DllImport("webshot64.dll", EntryPoint = "WebShot_SetDocumentCallback", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int SetDocumentCallback_64(IntPtr WebShotHandle, IntPtr UserHandle, DocumentCallback Callback);
   [DllImport("webshot.dll", EntryPoint = "WebShot_SetDocumentCallback", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int SetDocumentCallback_32(IntPtr WebShotHandle, IntPtr UserHandle, DocumentCallback Callback);
   [DllImport("webshot_svc64.dll", EntryPoint = "WebShot_SetDocumentCallback", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
   private static extern int SetDocumentCallback_svc64(IntPtr WebShotHandle, IntPtr UserHandle, DocumentCallback Callback);
   [DllImport("webshot_svc.dll", EntryPoint = "WebShot_SetDocumentCallback", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
   private static extern int SetDocumentCallback_svc32(IntPtr WebShotHandle, IntPtr UserHandle, DocumentCallback Callback);
   public static int SetDocumentCallback(IntPtr WebShotHandle, IntPtr UserHandle, DocumentCallback Callback)
   {
      var t = GetTable();
      switch (t)
      {
         case TableValue.x32_nosvc:
            return SetDocumentCallback_32(WebShotHandle, UserHandle, Callback);
         case TableValue.x32_svc:
            return SetDocumentCallback_svc32(WebShotHandle, UserHandle, Callback);
         case TableValue.x64_nosvc:
            return SetDocumentCallback_64(WebShotHandle, UserHandle, Callback);
         case TableValue.x64_svc:
            return SetDocumentCallback_svc64(WebShotHandle, UserHandle, Callback);
      }
      return -1;
   }

   [DllImport("webshot64.dll", EntryPoint = "WebShot_SetCaptureCallback", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int SetCaptureCallback_64(IntPtr WebShotHandle, IntPtr UserHandle, CaptureCallback Callback);
   [DllImport("webshot.dll", EntryPoint = "WebShot_SetCaptureCallback", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int SetCaptureCallback_32(IntPtr WebShotHandle, IntPtr UserHandle, CaptureCallback Callback);
   [DllImport("webshot_svc64.dll", EntryPoint = "WebShot_SetCaptureCallback", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
   private static extern int SetCaptureCallback_svc64(IntPtr WebShotHandle, IntPtr UserHandle, CaptureCallback Callback);
   [DllImport("webshot_svc.dll", EntryPoint = "WebShot_SetCaptureCallback", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
   private static extern int SetCaptureCallback_svc32(IntPtr WebShotHandle, IntPtr UserHandle, CaptureCallback Callback);
   public static int SetCaptureCallback(IntPtr WebShotHandle, IntPtr UserHandle, CaptureCallback Callback)
   {
      var t = GetTable();
      switch (t)
      {
         case TableValue.x32_nosvc:
            return SetCaptureCallback_32(WebShotHandle, UserHandle, Callback);
         case TableValue.x32_svc:
            return SetCaptureCallback_svc32(WebShotHandle, UserHandle, Callback);
         case TableValue.x64_nosvc:
            return SetCaptureCallback_64(WebShotHandle, UserHandle, Callback);
         case TableValue.x64_svc:
            return SetCaptureCallback_svc64(WebShotHandle, UserHandle, Callback);
      }
      return -1;
   }

   [DllImport("webshot64.dll", EntryPoint = "WebShot_SetExitCallback", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int SetExitCallback_64(IntPtr WebShotHandle, IntPtr UserHandle, ExitCallback Callback);
   [DllImport("webshot.dll", EntryPoint = "WebShot_SetExitCallback", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int SetExitCallback_32(IntPtr WebShotHandle, IntPtr UserHandle, ExitCallback Callback);
   public static int SetExitCallback(IntPtr WebShotHandle, IntPtr UserHandle, ExitCallback Callback)
   {
      if (Environment.Is64BitProcess)
         return SetExitCallback_64(WebShotHandle, UserHandle, Callback);
      else
         return SetExitCallback_32(WebShotHandle, UserHandle, Callback);
   }

   [DllImport("webshot64.dll", EntryPoint = "WebShot_GetParentWindow", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int GetParentWindow_64(IntPtr WebShotHandle, ref int hWnd);
   [DllImport("webshot.dll", EntryPoint = "WebShot_GetParentWindow", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int GetParentWindow_32(IntPtr WebShotHandle, ref int hWnd);
   public static int GetParentWindow(IntPtr WebShotHandle, ref int hWnd)
   {
      if (Environment.Is64BitProcess)
         return GetParentWindow_64(WebShotHandle, ref hWnd);
      else
         return GetParentWindow_32(WebShotHandle, ref hWnd);
   }

   [DllImport("webshot64.dll", EntryPoint = "WebShot_SetParentWindow", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int SetParentWindow_64(IntPtr WebShotHandle, int hWnd);
   [DllImport("webshot.dll", EntryPoint = "WebShot_SetParentWindow", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int SetParentWindow_32(IntPtr WebShotHandle, int hWnd);
   public static int SetParentWindow(IntPtr WebShotHandle, int hWnd)
   {
      if (Environment.Is64BitProcess)
         return SetParentWindow_64(WebShotHandle, hWnd);
      else
         return SetParentWindow_32(WebShotHandle, hWnd);
   }

   [DllImport("webshot64.dll", EntryPoint = "WebShot_GetOutputPath", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int GetOutputPath_64(IntPtr WebShotHandle, StringBuilder Path, int MaxPath);
   [DllImport("webshot.dll", EntryPoint = "WebShot_GetOutputPath", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int GetOutputPath_32(IntPtr WebShotHandle, StringBuilder Path, int MaxPath);
   [DllImport("webshot_svc64.dll", EntryPoint = "WebShot_GetOutputPath", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
   private static extern int GetOutputPath_svc64(IntPtr WebShotHandle, StringBuilder Path, int MaxPath);
   [DllImport("webshot_svc.dll", EntryPoint = "WebShot_GetOutputPath", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
   private static extern int GetOutputPath_svc32(IntPtr WebShotHandle, StringBuilder Path, int MaxPath);
   public static int GetOutputPath(IntPtr WebShotHandle, StringBuilder Path, int MaxPath)
   {
      var t = GetTable();
      switch (t)
      {
         case TableValue.x32_nosvc:
            return GetOutputPath_32(WebShotHandle, Path, MaxPath);
         case TableValue.x32_svc:
            return GetOutputPath_svc32(WebShotHandle, Path, MaxPath);
         case TableValue.x64_nosvc:
            return GetOutputPath_64(WebShotHandle, Path, MaxPath);
         case TableValue.x64_svc:
            return GetOutputPath_svc64(WebShotHandle, Path, MaxPath);
      }
      return -1;
   }

   [DllImport("webshot64.dll", EntryPoint = "WebShot_SetOutputPath", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int SetOutputPath_64(IntPtr WebShotHandle, string Path);
   [DllImport("webshot.dll", EntryPoint = "WebShot_SetOutputPath", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int SetOutputPath_32(IntPtr WebShotHandle, string Path);
   [DllImport("webshot_svc64.dll", EntryPoint = "WebShot_SetOutputPath", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
   private static extern int SetOutputPath_svc64(IntPtr WebShotHandle, string Path);
   [DllImport("webshot_svc.dll", EntryPoint = "WebShot_SetOutputPath", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
   private static extern int SetOutputPath_svc32(IntPtr WebShotHandle, string Path);
   public static int SetOutputPath(IntPtr WebShotHandle, string Path)
   {
      var t = GetTable();
      switch (t)
      {
         case TableValue.x32_nosvc:
            return SetOutputPath_32(WebShotHandle, Path);
         case TableValue.x32_svc:
            return SetOutputPath_svc32(WebShotHandle, Path);
         case TableValue.x64_nosvc:
            return SetOutputPath_64(WebShotHandle, Path);
         case TableValue.x64_svc:
            return SetOutputPath_svc64(WebShotHandle, Path);
      }
      return -1;
   }

   [DllImport("webshot64.dll", EntryPoint = "WebShot_GetPageTimeout", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int GetPageTimeout_64(IntPtr WebShotHandle, ref int Seconds);
   [DllImport("webshot.dll", EntryPoint = "WebShot_GetPageTimeout", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int GetPageTimeout_32(IntPtr WebShotHandle, ref int Seconds);
   [DllImport("webshot_svc64.dll", EntryPoint = "WebShot_GetPageTimeout", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
   private static extern int GetPageTimeout_svc64(IntPtr WebShotHandle, ref int Seconds);
   [DllImport("webshot_svc.dll", EntryPoint = "WebShot_GetPageTimeout", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
   private static extern int GetPageTimeout_svc32(IntPtr WebShotHandle, ref int Seconds);
   public static int GetPageTimeout(IntPtr WebShotHandle, ref int Seconds)
   {
      var t = GetTable();
      switch (t)
      {
         case TableValue.x32_nosvc:
            return GetPageTimeout_32(WebShotHandle, ref Seconds);
         case TableValue.x32_svc:
            return GetPageTimeout_svc32(WebShotHandle, ref Seconds);
         case TableValue.x64_nosvc:
            return GetPageTimeout_64(WebShotHandle, ref Seconds);
         case TableValue.x64_svc:
            return GetPageTimeout_svc64(WebShotHandle, ref Seconds);
      }
      return -1;
   }

   [DllImport("webshot64.dll", EntryPoint = "WebShot_SetPageTimeout", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int SetPageTimeout_64(IntPtr WebShotHandle, int Seconds);
   [DllImport("webshot.dll", EntryPoint = "WebShot_SetPageTimeout", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int SetPageTimeout_32(IntPtr WebShotHandle, int Seconds);
   [DllImport("webshot_svc64.dll", EntryPoint = "WebShot_SetPageTimeout", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
   private static extern int SetPageTimeout_svc64(IntPtr WebShotHandle, int Seconds);
   [DllImport("webshot_svc.dll", EntryPoint = "WebShot_SetPageTimeout", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
   private static extern int SetPageTimeout_svc32(IntPtr WebShotHandle, int Seconds);
   public static int SetPageTimeout(IntPtr WebShotHandle, int Seconds)
   {
      var t = GetTable();
      switch (t)
      {
         case TableValue.x32_nosvc:
            return SetPageTimeout_32(WebShotHandle, Seconds);
         case TableValue.x32_svc:
            return SetPageTimeout_svc32(WebShotHandle, Seconds);
         case TableValue.x64_nosvc:
            return SetPageTimeout_64(WebShotHandle, Seconds);
         case TableValue.x64_svc:
            return SetPageTimeout_svc64(WebShotHandle, Seconds);
      }
      return -1;
   }

   [DllImport("webshot64.dll", EntryPoint = "WebShot_GetMetaRefreshTimeout", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int GetMetaRefreshTimeout_64(IntPtr WebShotHandle, ref int Seconds);
   [DllImport("webshot.dll", EntryPoint = "WebShot_GetMetaRefreshTimeout", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int GetMetaRefreshTimeout_32(IntPtr WebShotHandle, ref int Seconds);
   [DllImport("webshot_svc64.dll", EntryPoint = "WebShot_GetMetaRefreshTimeout", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
   private static extern int GetMetaRefreshTimeout_svc64(IntPtr WebShotHandle, ref int Seconds);
   [DllImport("webshot_svc.dll", EntryPoint = "WebShot_GetMetaRefreshTimeout", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
   private static extern int GetMetaRefreshTimeout_svc32(IntPtr WebShotHandle, ref int Seconds);
   public static int GetMetaRefreshTimeout(IntPtr WebShotHandle, ref int Seconds)
   {
      var t = GetTable();
      switch (t)
      {
         case TableValue.x32_nosvc:
            return GetMetaRefreshTimeout_32(WebShotHandle, ref Seconds);
         case TableValue.x32_svc:
            return GetMetaRefreshTimeout_svc32(WebShotHandle, ref Seconds);
         case TableValue.x64_nosvc:
            return GetMetaRefreshTimeout_64(WebShotHandle, ref Seconds);
         case TableValue.x64_svc:
            return GetMetaRefreshTimeout_svc64(WebShotHandle, ref Seconds);
      }
      return -1;
   }

   [DllImport("webshot64.dll", EntryPoint = "WebShot_SetMetaRefreshTimeout", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int SetMetaRefreshTimeout_64(IntPtr WebShotHandle, int Seconds);
   [DllImport("webshot.dll", EntryPoint = "WebShot_SetMetaRefreshTimeout", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int SetMetaRefreshTimeout_32(IntPtr WebShotHandle, int Seconds);
   [DllImport("webshot_svc64.dll", EntryPoint = "WebShot_SetMetaRefreshTimeout", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
   private static extern int SetMetaRefreshTimeout_svc64(IntPtr WebShotHandle, int Seconds);
   [DllImport("webshot_svc.dll", EntryPoint = "WebShot_SetMetaRefreshTimeout", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
   private static extern int SetMetaRefreshTimeout_svc32(IntPtr WebShotHandle, int Seconds);
   public static int SetMetaRefreshTimeout(IntPtr WebShotHandle, int Seconds)
   {
      var t = GetTable();
      switch (t)
      {
         case TableValue.x32_nosvc:
            return SetMetaRefreshTimeout_32(WebShotHandle, Seconds);
         case TableValue.x32_svc:
            return SetMetaRefreshTimeout_svc32(WebShotHandle, Seconds);
         case TableValue.x64_nosvc:
            return SetMetaRefreshTimeout_64(WebShotHandle, Seconds);
         case TableValue.x64_svc:
            return SetMetaRefreshTimeout_svc64(WebShotHandle, Seconds);
      }
      return -1;
   }

   [DllImport("webshot64.dll", EntryPoint = "WebShot_GetDocumentWait", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int GetDocumentWait_64(IntPtr WebShotHandle, ref int Seconds);
   [DllImport("webshot.dll", EntryPoint = "WebShot_GetDocumentWait", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int GetDocumentWait_32(IntPtr WebShotHandle, ref int Seconds);
   [DllImport("webshot_svc64.dll", EntryPoint = "WebShot_GetDocumentWait", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
   private static extern int GetDocumentWait_svc64(IntPtr WebShotHandle, ref int Seconds);
   [DllImport("webshot_svc.dll", EntryPoint = "WebShot_GetDocumentWait", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
   private static extern int GetDocumentWait_svc32(IntPtr WebShotHandle, ref int Seconds);
   public static int GetDocumentWait(IntPtr WebShotHandle, ref int Seconds)
   {
      var t = GetTable();
      switch (t)
      {
         case TableValue.x32_nosvc:
            return GetDocumentWait_32(WebShotHandle, ref Seconds);
         case TableValue.x32_svc:
            return GetDocumentWait_svc32(WebShotHandle, ref Seconds);
         case TableValue.x64_nosvc:
            return GetDocumentWait_64(WebShotHandle, ref Seconds);
         case TableValue.x64_svc:
            return GetDocumentWait_svc64(WebShotHandle, ref Seconds);
      }
      return -1;
   }

   [DllImport("webshot64.dll", EntryPoint = "WebShot_SetDocumentWait", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int SetDocumentWait_64(IntPtr WebShotHandle, int Seconds);
   [DllImport("webshot.dll", EntryPoint = "WebShot_SetDocumentWait", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int SetDocumentWait_32(IntPtr WebShotHandle, int Seconds);
   [DllImport("webshot_svc64.dll", EntryPoint = "WebShot_SetDocumentWait", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
   private static extern int SetDocumentWait_svc64(IntPtr WebShotHandle, int Seconds);
   [DllImport("webshot_svc.dll", EntryPoint = "WebShot_SetDocumentWait", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
   private static extern int SetDocumentWait_svc32(IntPtr WebShotHandle, int Seconds);
   public static int SetDocumentWait(IntPtr WebShotHandle, int Seconds)
   {
      var t = GetTable();
      switch (t)
      {
         case TableValue.x32_nosvc:
            return SetDocumentWait_32(WebShotHandle, Seconds);
         case TableValue.x32_svc:
            return SetDocumentWait_svc32(WebShotHandle, Seconds);
         case TableValue.x64_nosvc:
            return SetDocumentWait_64(WebShotHandle, Seconds);
         case TableValue.x64_svc:
            return SetDocumentWait_svc64(WebShotHandle, Seconds);
      }
      return -1;
   }
   
   [DllImport("webshot64.dll", EntryPoint = "WebShot_GetDocumentWaitFlash", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int GetDocumentWaitFlash_64(IntPtr WebShotHandle, ref int Seconds);
   [DllImport("webshot.dll", EntryPoint = "WebShot_GetDocumentWaitFlash", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int GetDocumentWaitFlash_32(IntPtr WebShotHandle, ref int Seconds);
   [DllImport("webshot_svc64.dll", EntryPoint = "WebShot_GetDocumentWaitFlash", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
   private static extern int GetDocumentWaitFlash_svc64(IntPtr WebShotHandle, ref int Seconds);
   [DllImport("webshot_svc.dll", EntryPoint = "WebShot_GetDocumentWaitFlash", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
   private static extern int GetDocumentWaitFlash_svc32(IntPtr WebShotHandle, ref int Seconds);
   public static int GetDocumentWaitFlash(IntPtr WebShotHandle, ref int Seconds)
   {
      var t = GetTable();
      switch (t)
      {
         case TableValue.x32_nosvc:
            return GetDocumentWaitFlash_32(WebShotHandle, ref Seconds);
         case TableValue.x32_svc:
            return GetDocumentWaitFlash_svc32(WebShotHandle, ref Seconds);
         case TableValue.x64_nosvc:
            return GetDocumentWaitFlash_64(WebShotHandle, ref Seconds);
         case TableValue.x64_svc:
            return GetDocumentWaitFlash_svc64(WebShotHandle, ref Seconds);
      }
      return -1;
   }

   [DllImport("webshot64.dll", EntryPoint = "WebShot_SetDocumentWaitFlash", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int SetDocumentWaitFlash_64(IntPtr WebShotHandle, int Seconds);
   [DllImport("webshot.dll", EntryPoint = "WebShot_SetDocumentWaitFlash", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int SetDocumentWaitFlash_32(IntPtr WebShotHandle, int Seconds);
   [DllImport("webshot_svc64.dll", EntryPoint = "WebShot_SetDocumentWaitFlash", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
   private static extern int SetDocumentWaitFlash_svc64(IntPtr WebShotHandle, int Seconds);
   [DllImport("webshot_svc.dll", EntryPoint = "WebShot_SetDocumentWaitFlash", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
   private static extern int SetDocumentWaitFlash_svc32(IntPtr WebShotHandle, int Seconds);
   public static int SetDocumentWaitFlash(IntPtr WebShotHandle, int Seconds)
   {
      var t = GetTable();
      switch (t)
      {
         case TableValue.x32_nosvc:
            return SetDocumentWaitFlash_32(WebShotHandle, Seconds);
         case TableValue.x32_svc:
            return SetDocumentWaitFlash_svc32(WebShotHandle, Seconds);
         case TableValue.x64_nosvc:
            return SetDocumentWaitFlash_64(WebShotHandle, Seconds);
         case TableValue.x64_svc:
            return SetDocumentWaitFlash_svc64(WebShotHandle, Seconds);
      }
      return -1;
   }

   [DllImport("webshot64.dll", EntryPoint = "WebShot_GetImageWait", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int GetImageWait_64(IntPtr WebShotHandle, ref int Seconds);
   [DllImport("webshot.dll", EntryPoint = "WebShot_GetImageWait", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int GetImageWait_32(IntPtr WebShotHandle, ref int Seconds);
   [DllImport("webshot_svc64.dll", EntryPoint = "WebShot_GetImageWait", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
   private static extern int GetImageWait_svc64(IntPtr WebShotHandle, ref int Seconds);
   [DllImport("webshot_svc.dll", EntryPoint = "WebShot_GetImageWait", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
   private static extern int GetImageWait_svc32(IntPtr WebShotHandle, ref int Seconds);
   public static int GetImageWait(IntPtr WebShotHandle, ref int Seconds)
   {
      var t = GetTable();
      switch (t)
      {
         case TableValue.x32_nosvc:
            return GetImageWait_32(WebShotHandle, ref Seconds);
         case TableValue.x32_svc:
            return GetImageWait_svc32(WebShotHandle, ref Seconds);
         case TableValue.x64_nosvc:
            return GetImageWait_64(WebShotHandle, ref Seconds);
         case TableValue.x64_svc:
            return GetImageWait_svc64(WebShotHandle, ref Seconds);
      }
      return -1;
   }

   [DllImport("webshot64.dll", EntryPoint = "WebShot_SetImageWait", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int SetImageWait_64(IntPtr WebShotHandle, int Seconds);
   [DllImport("webshot.dll", EntryPoint = "WebShot_SetImageWait", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int SetImageWait_32(IntPtr WebShotHandle, int Seconds);
   [DllImport("webshot_svc64.dll", EntryPoint = "WebShot_SetImageWait", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
   private static extern int SetImageWait_svc64(IntPtr WebShotHandle, int Seconds);
   [DllImport("webshot_svc.dll", EntryPoint = "WebShot_SetImageWait", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
   private static extern int SetImageWait_svc32(IntPtr WebShotHandle, int Seconds);
   public static int SetImageWait(IntPtr WebShotHandle, int Seconds)
   {
      var t = GetTable();
      switch (t)
      {
         case TableValue.x32_nosvc:
            return SetImageWait_32(WebShotHandle, Seconds);
         case TableValue.x32_svc:
            return SetImageWait_svc32(WebShotHandle, Seconds);
         case TableValue.x64_nosvc:
            return SetImageWait_64(WebShotHandle, Seconds);
         case TableValue.x64_svc:
            return SetImageWait_svc64(WebShotHandle, Seconds);
      }
      return -1;
   }

   [DllImport("webshot64.dll", EntryPoint = "WebShot_GetImageWaitLoop", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int GetImageWaitLoop_64(IntPtr WebShotHandle, ref int Count);
   [DllImport("webshot.dll", EntryPoint = "WebShot_GetImageWaitLoop", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int GetImageWaitLoop_32(IntPtr WebShotHandle, ref int Count);
   public static int GetImageWaitLoop(IntPtr WebShotHandle, ref int Count)
   {
      if (Environment.Is64BitProcess)
         return GetImageWaitLoop_64(WebShotHandle, ref Count);
      else
         return GetImageWaitLoop_32(WebShotHandle, ref Count);
   }

   [DllImport("webshot64.dll", EntryPoint = "WebShot_SetImageWaitLoop", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int SetImageWaitLoop_64(IntPtr WebShotHandle, int Count);
   [DllImport("webshot.dll", EntryPoint = "WebShot_SetImageWaitLoop", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int SetImageWaitLoop_32(IntPtr WebShotHandle, int Count);
   public static int SetImageWaitLoop(IntPtr WebShotHandle, int Count)
   {
      if (Environment.Is64BitProcess)
         return SetImageWaitLoop_64(WebShotHandle, Count);
      else
         return SetImageWaitLoop_32(WebShotHandle, Count);
   }

   [DllImport("webshot64.dll", EntryPoint = "WebShot_GetRedirectMaximum", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int GetRedirectMaximum_64(IntPtr WebShotHandle, ref int MaxRedirects);
   [DllImport("webshot.dll", EntryPoint = "WebShot_GetRedirectMaximum", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int GetRedirectMaximum_32(IntPtr WebShotHandle, ref int MaxRedirects);
   [DllImport("webshot_svc64.dll", EntryPoint = "WebShot_GetRedirectMaximum", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
   private static extern int GetRedirectMaximum_svc64(IntPtr WebShotHandle, ref int MaxRedirects);
   [DllImport("webshot_svc.dll", EntryPoint = "WebShot_GetRedirectMaximum", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
   private static extern int GetRedirectMaximum_svc32(IntPtr WebShotHandle, ref int MaxRedirects);
   public static int GetRedirectMaximum(IntPtr WebShotHandle, ref int MaxRedirects)
   {
      var t = GetTable();
      switch (t)
      {
         case TableValue.x32_nosvc:
            return GetRedirectMaximum_32(WebShotHandle, ref MaxRedirects);
         case TableValue.x32_svc:
            return GetRedirectMaximum_svc32(WebShotHandle, ref MaxRedirects);
         case TableValue.x64_nosvc:
            return GetRedirectMaximum_64(WebShotHandle, ref MaxRedirects);
         case TableValue.x64_svc:
            return GetRedirectMaximum_svc64(WebShotHandle, ref MaxRedirects);
      }
      return -1;
   }

   [DllImport("webshot64.dll", EntryPoint = "WebShot_SetRedirectMaximum", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int SetRedirectMaximum_64(IntPtr WebShotHandle, int MaxRedirects);
   [DllImport("webshot.dll", EntryPoint = "WebShot_SetRedirectMaximum", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int SetRedirectMaximum_32(IntPtr WebShotHandle, int MaxRedirects);
   [DllImport("webshot_svc64.dll", EntryPoint = "WebShot_SetRedirectMaximum", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
   private static extern int SetRedirectMaximum_svc64(IntPtr WebShotHandle, int MaxRedirects);
   [DllImport("webshot_svc.dll", EntryPoint = "WebShot_SetRedirectMaximum", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
   private static extern int SetRedirectMaximum_svc32(IntPtr WebShotHandle, int MaxRedirects);
   public static int SetRedirectMaximum(IntPtr WebShotHandle, int MaxRedirects)
   {
      var t = GetTable();
      switch (t)
      {
         case TableValue.x32_nosvc:
            return SetRedirectMaximum_32(WebShotHandle, MaxRedirects);
         case TableValue.x32_svc:
            return SetRedirectMaximum_svc32(WebShotHandle, MaxRedirects);
         case TableValue.x64_nosvc:
            return SetRedirectMaximum_64(WebShotHandle, MaxRedirects);
         case TableValue.x64_svc:
            return SetRedirectMaximum_svc64(WebShotHandle, MaxRedirects);
      }
      return -1;
   }

   [DllImport("webshot64.dll", EntryPoint = "WebShot_GetRedirectCount", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int GetRedirectCount_64(IntPtr WebShotHandle, ref int Count);
   [DllImport("webshot.dll", EntryPoint = "WebShot_GetRedirectCount", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int GetRedirectCount_32(IntPtr WebShotHandle, ref int Count);
   [DllImport("webshot_svc64.dll", EntryPoint = "WebShot_GetRedirectCount", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
   private static extern int GetRedirectCount_svc64(IntPtr WebShotHandle, ref int Count);
   [DllImport("webshot_svc.dll", EntryPoint = "WebShot_GetRedirectCount", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
   private static extern int GetRedirectCount_svc32(IntPtr WebShotHandle, ref int Count);
   public static int GetRedirectCount(IntPtr WebShotHandle, ref int Count)
   {
      var t = GetTable();
      switch (t)
      {
         case TableValue.x32_nosvc:
            return GetRedirectCount_32(WebShotHandle, ref Count);
         case TableValue.x32_svc:
            return GetRedirectCount_svc32(WebShotHandle, ref Count);
         case TableValue.x64_nosvc:
            return GetRedirectCount_64(WebShotHandle, ref Count);
         case TableValue.x64_svc:
            return GetRedirectCount_svc64(WebShotHandle, ref Count);
      }
      return -1;
   }

   [DllImport("webshot64.dll", EntryPoint = "WebShot_GetRedirectUrl", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int GetRedirectUrl_64(IntPtr WebShotHandle, StringBuilder RedirectUrl, int MaxRedirectUrl);
   [DllImport("webshot.dll", EntryPoint = "WebShot_GetRedirectUrl", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int GetRedirectUrl_32(IntPtr WebShotHandle, StringBuilder RedirectUrl, int MaxRedirectUrl);
   [DllImport("webshot_svc64.dll", EntryPoint = "WebShot_GetRedirectUrl", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
   private static extern int GetRedirectUrl_svc64(IntPtr WebShotHandle, StringBuilder RedirectUrl, int MaxRedirectUrl);
   [DllImport("webshot_svc.dll", EntryPoint = "WebShot_GetRedirectUrl", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
   private static extern int GetRedirectUrl_svc32(IntPtr WebShotHandle, StringBuilder RedirectUrl, int MaxRedirectUrl);
   public static int GetRedirectUrl(IntPtr WebShotHandle, StringBuilder RedirectUrl, int MaxRedirectUrl)
   {
      var t = GetTable();
      switch (t)
      {
         case TableValue.x32_nosvc:
            return GetRedirectUrl_32(WebShotHandle, RedirectUrl, MaxRedirectUrl);
         case TableValue.x32_svc:
            return GetRedirectUrl_svc32(WebShotHandle, RedirectUrl, MaxRedirectUrl);
         case TableValue.x64_nosvc:
            return GetRedirectUrl_64(WebShotHandle, RedirectUrl, MaxRedirectUrl);
         case TableValue.x64_svc:
            return GetRedirectUrl_svc64(WebShotHandle, RedirectUrl, MaxRedirectUrl);
      }
      return -1;
   }

   [DllImport("webshot64.dll", EntryPoint = "WebShot_GetUrl", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int GetUrl_64(IntPtr WebShotHandle, StringBuilder Url, int MaxUrl);
   [DllImport("webshot.dll", EntryPoint = "WebShot_GetUrl", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int GetUrl_32(IntPtr WebShotHandle, StringBuilder Url, int MaxUrl);
   [DllImport("webshot_svc64.dll", EntryPoint = "WebShot_GetUrl", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
   private static extern int GetUrl_svc64(IntPtr WebShotHandle, StringBuilder Url, int MaxUrl);
   [DllImport("webshot_svc.dll", EntryPoint = "WebShot_GetUrl", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
   private static extern int GetUrl_svc32(IntPtr WebShotHandle, StringBuilder Url, int MaxUrl);
   public static int GetUrl(IntPtr WebShotHandle, StringBuilder Url, int MaxUrl)
   {
      var t = GetTable();
      switch (t)
      {
         case TableValue.x32_nosvc:
            return GetUrl_32(WebShotHandle, Url, MaxUrl);
         case TableValue.x32_svc:
            return GetUrl_svc32(WebShotHandle, Url, MaxUrl);
         case TableValue.x64_nosvc:
            return GetUrl_64(WebShotHandle, Url, MaxUrl);
         case TableValue.x64_svc:
            return GetUrl_svc64(WebShotHandle, Url, MaxUrl);
      }
      return -1;
   }

   [DllImport("webshot64.dll", EntryPoint = "WebShot_GetTitle", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int GetTitle_64(IntPtr WebShotHandle, StringBuilder Title, int MaxTitle);
   [DllImport("webshot.dll", EntryPoint = "WebShot_GetTitle", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int GetTitle_32(IntPtr WebShotHandle, StringBuilder Title, int MaxTitle);
   public static int GetTitle(IntPtr WebShotHandle, StringBuilder Title, int MaxTitle)
   {
      if (Environment.Is64BitProcess)
         return GetTitle_64(WebShotHandle, Title, MaxTitle);
      else
         return GetTitle_32(WebShotHandle, Title, MaxTitle);
   }   

   [DllImport("webshot64.dll", EntryPoint = "WebShot_SetTitle", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int SetTitle_64(IntPtr WebShotHandle, string Title);
   [DllImport("webshot.dll", EntryPoint = "WebShot_SetTitle", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int SetTitle_32(IntPtr WebShotHandle, string Title);
   public static int SetTitle(IntPtr WebShotHandle, string Title)
   {
      if (Environment.Is64BitProcess)
         return SetTitle_64(WebShotHandle, Title);
      else
         return SetTitle_32(WebShotHandle, Title);
   }

   [DllImport("webshot64.dll", EntryPoint = "WebShot_GetMetaKeywords", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int GetMetaKeywords_64(IntPtr WebShotHandle, StringBuilder MetaKeywords, int MaxMetaKeywords);
   [DllImport("webshot.dll", EntryPoint = "WebShot_GetMetaKeywords", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int GetMetaKeywords_32(IntPtr WebShotHandle, StringBuilder MetaKeywords, int MaxMetaKeywords);
   public static int GetMetaKeywords(IntPtr WebShotHandle, StringBuilder MetaKeywords, int MaxMetaKeywords)
   {
      if (Environment.Is64BitProcess)
         return GetMetaKeywords_64(WebShotHandle, MetaKeywords, MaxMetaKeywords);
      else
         return GetMetaKeywords_32(WebShotHandle, MetaKeywords, MaxMetaKeywords);
   }

   [DllImport("webshot64.dll", EntryPoint = "WebShot_GetMetaDescription", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int GetMetaDescription_64(IntPtr WebShotHandle, StringBuilder MetaDescription, int MaxMetaDescription);
   [DllImport("webshot.dll", EntryPoint = "WebShot_GetMetaDescription", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int GetMetaDescription_32(IntPtr WebShotHandle, StringBuilder MetaDescription, int MaxMetaDescription);
   public static int GetMetaDescription(IntPtr WebShotHandle, StringBuilder MetaDescription, int MaxMetaDescription)
   {
      if (Environment.Is64BitProcess)
         return GetMetaDescription_64(WebShotHandle, MetaDescription, MaxMetaDescription);
      else
         return GetMetaDescription_32(WebShotHandle, MetaDescription, MaxMetaDescription);
   }

   [DllImport("webshot64.dll", EntryPoint = "WebShot_GetUserAgent", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int GetUserAgent_64(IntPtr WebShotHandle, StringBuilder UserAgent, int MaxUserAgent);
   [DllImport("webshot.dll", EntryPoint = "WebShot_GetUserAgent", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int GetUserAgent_32(IntPtr WebShotHandle, StringBuilder UserAgent, int MaxUserAgent);
   public static int GetUserAgent(IntPtr WebShotHandle, StringBuilder UserAgent, int MaxUserAgent)
   {
      if (Environment.Is64BitProcess)
         return GetUserAgent_64(WebShotHandle, UserAgent, MaxUserAgent);
      else
         return GetUserAgent_32(WebShotHandle, UserAgent, MaxUserAgent);
   }
   
   [DllImport("webshot64.dll", EntryPoint = "WebShot_SetUserAgent", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int SetUserAgent_64(IntPtr WebShotHandle, string UserAgent);
   [DllImport("webshot.dll", EntryPoint = "WebShot_SetUserAgent", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int SetUserAgent_32(IntPtr WebShotHandle, string UserAgent);
   public static int SetUserAgent(IntPtr WebShotHandle, string UserAgent)
   {
      if (Environment.Is64BitProcess)
         return SetUserAgent_64(WebShotHandle, UserAgent);
      else
         return SetUserAgent_32(WebShotHandle, UserAgent);
   }

   [DllImport("webshot64.dll", EntryPoint = "WebShot_GetCustomHeaders", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int GetCustomHeaders_64(IntPtr WebShotHandle, StringBuilder CustomHeaders, int MaxCustomHeaders);
   [DllImport("webshot.dll", EntryPoint = "WebShot_GetCustomHeaders", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int GetCustomHeaders_32(IntPtr WebShotHandle, StringBuilder CustomHeaders, int MaxCustomHeaders);
   [DllImport("webshot_svc64.dll", EntryPoint = "WebShot_GetCustomHeaders", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
   private static extern int GetCustomHeaders_svc64(IntPtr WebShotHandle, StringBuilder CustomHeaders, int MaxCustomHeaders);
   [DllImport("webshot_svc.dll", EntryPoint = "WebShot_GetCustomHeaders", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
   private static extern int GetCustomHeaders_svc32(IntPtr WebShotHandle, StringBuilder CustomHeaders, int MaxCustomHeaders);
   public static int GetCustomHeaders(IntPtr WebShotHandle, StringBuilder CustomHeaders, int MaxCustomHeaders)
   {
      var t = GetTable();
      switch (t)
      {
         case TableValue.x32_nosvc:
            return GetCustomHeaders_32(WebShotHandle, CustomHeaders, MaxCustomHeaders);
         case TableValue.x32_svc:
            return GetCustomHeaders_svc32(WebShotHandle, CustomHeaders, MaxCustomHeaders);
         case TableValue.x64_nosvc:
            return GetCustomHeaders_64(WebShotHandle, CustomHeaders, MaxCustomHeaders);
         case TableValue.x64_svc:
            return GetCustomHeaders_svc64(WebShotHandle, CustomHeaders, MaxCustomHeaders);
      }
      return -1;
   }

   [DllImport("webshot64.dll", EntryPoint = "WebShot_SetCustomHeaders", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int SetCustomHeaders_64(IntPtr WebShotHandle, string CustomHeaders);
   [DllImport("webshot.dll", EntryPoint = "WebShot_SetCustomHeaders", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int SetCustomHeaders_32(IntPtr WebShotHandle, string CustomHeaders);
   [DllImport("webshot_svc64.dll", EntryPoint = "WebShot_SetCustomHeaders", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
   private static extern int SetCustomHeaders_svc64(IntPtr WebShotHandle, string CustomHeaders);
   [DllImport("webshot_svc.dll", EntryPoint = "WebShot_SetCustomHeaders", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
   private static extern int SetCustomHeaders_svc32(IntPtr WebShotHandle, string CustomHeaders);
   public static int SetCustomHeaders(IntPtr WebShotHandle, string CustomHeaders)
   {
      var t = GetTable();
      switch (t)
      {
         case TableValue.x32_nosvc:
            return SetCustomHeaders_32(WebShotHandle, CustomHeaders);
         case TableValue.x32_svc:
            return SetCustomHeaders_svc32(WebShotHandle, CustomHeaders);
         case TableValue.x64_nosvc:
            return SetCustomHeaders_64(WebShotHandle, CustomHeaders);
         case TableValue.x64_svc:
            return SetCustomHeaders_svc64(WebShotHandle, CustomHeaders);
      }
      return -1;
   }

   [DllImport("webshot64.dll", EntryPoint = "WebShot_GetResponseHeaders", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int GetResponseHeaders_64(IntPtr WebShotHandle, StringBuilder ResponseHeaders, int MaxResponseHeaders);
   [DllImport("webshot.dll", EntryPoint = "WebShot_GetResponseHeaders", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int GetResponseHeaders_32(IntPtr WebShotHandle, StringBuilder ResponseHeaders, int MaxResponseHeaders);
   public static int GetResponseHeaders(IntPtr WebShotHandle, StringBuilder ResponseHeaders, int MaxResponseHeaders)
   {
      if (Environment.Is64BitProcess)
         return GetResponseHeaders_64(WebShotHandle, ResponseHeaders, MaxResponseHeaders);
      else
         return GetResponseHeaders_32(WebShotHandle, ResponseHeaders, MaxResponseHeaders);
   }
   
   [DllImport("webshot64.dll", EntryPoint = "WebShot_GetPostData", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int GetPostData_64(IntPtr WebShotHandle, StringBuilder PostData, int MaxPostData);
   [DllImport("webshot.dll", EntryPoint = "WebShot_GetPostData", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int GetPostData_32(IntPtr WebShotHandle, StringBuilder PostData, int MaxPostData);
   [DllImport("webshot_svc64.dll", EntryPoint = "WebShot_GetPostData", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
   private static extern int GetPostData_svc64(IntPtr WebShotHandle, StringBuilder PostData, int MaxPostData);
   [DllImport("webshot_svc.dll", EntryPoint = "WebShot_GetPostData", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
   private static extern int GetPostData_svc32(IntPtr WebShotHandle, StringBuilder PostData, int MaxPostData);
   public static int GetPostData(IntPtr WebShotHandle, StringBuilder PostData, int MaxPostData)
   {
      var t = GetTable();
      switch (t)
      {
         case TableValue.x32_nosvc:
            return GetPostData_32(WebShotHandle, PostData, MaxPostData);
         case TableValue.x32_svc:
            return GetPostData_svc32(WebShotHandle, PostData, MaxPostData);
         case TableValue.x64_nosvc:
            return GetPostData_64(WebShotHandle, PostData, MaxPostData);
         case TableValue.x64_svc:
            return GetPostData_svc64(WebShotHandle, PostData, MaxPostData);
      }
      return -1;
   }

   [DllImport("webshot64.dll", EntryPoint = "WebShot_SetPostData", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int SetPostData_64(IntPtr WebShotHandle, string PostData);
   [DllImport("webshot.dll", EntryPoint = "WebShot_SetPostData", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int SetPostData_32(IntPtr WebShotHandle, string PostData);
   [DllImport("webshot_svc64.dll", EntryPoint = "WebShot_SetPostData", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
   private static extern int SetPostData_svc64(IntPtr WebShotHandle, string PostData);
   [DllImport("webshot_svc.dll", EntryPoint = "WebShot_SetPostData", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
   private static extern int SetPostData_svc32(IntPtr WebShotHandle, string PostData);
   public static int SetPostData(IntPtr WebShotHandle, string PostData)
   {
      var t = GetTable();
      switch (t)
      {
         case TableValue.x32_nosvc:
            return SetPostData_32(WebShotHandle, PostData);
         case TableValue.x32_svc:
            return SetPostData_svc32(WebShotHandle, PostData);
         case TableValue.x64_nosvc:
            return SetPostData_64(WebShotHandle, PostData);
         case TableValue.x64_svc:
            return SetPostData_svc64(WebShotHandle, PostData);
      }
      return -1;
   }

   [DllImport("webshot64.dll", EntryPoint = "WebShot_SetUsername", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int SetUsername_64(IntPtr WebShotHandle, string Username);
   [DllImport("webshot.dll", EntryPoint = "WebShot_SetUsername", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int SetUsername_32(IntPtr WebShotHandle, string Username);
   public static int SetUsername(IntPtr WebShotHandle, string Username)
   {
      if (Environment.Is64BitProcess)
         return SetUsername_64(WebShotHandle, Username);
      else
         return SetUsername_32(WebShotHandle, Username);
   }

   [DllImport("webshot64.dll", EntryPoint = "WebShot_SetPassword", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int SetPassword_64(IntPtr WebShotHandle, string Password);
   [DllImport("webshot.dll", EntryPoint = "WebShot_SetPassword", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int SetPassword_32(IntPtr WebShotHandle, string Password);
   public static int SetPassword(IntPtr WebShotHandle, string Password)
   {
      if (Environment.Is64BitProcess)
         return SetPassword_64(WebShotHandle, Password);
      else
         return SetPassword_32(WebShotHandle, Password);
   }

   [DllImport("webshot64.dll", EntryPoint = "WebShot_GetDisableActiveX", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int GetDisableActiveX_64(IntPtr WebShotHandle, ref int DisableActiveX);
   [DllImport("webshot.dll", EntryPoint = "WebShot_GetDisableActiveX", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int GetDisableActiveX_32(IntPtr WebShotHandle, ref int DisableActiveX);
   public static int GetDisableActiveX(IntPtr WebShotHandle, ref int DisableActiveX)
   {
      if (Environment.Is64BitProcess)
         return GetDisableActiveX_64(WebShotHandle, ref DisableActiveX);
      else
         return GetDisableActiveX_32(WebShotHandle, ref DisableActiveX);
   }
   
   [DllImport("webshot64.dll", EntryPoint = "WebShot_SetDisableActiveX", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int SetDisableActiveX_64(IntPtr WebShotHandle, int DisableActiveX);
   [DllImport("webshot.dll", EntryPoint = "WebShot_SetDisableActiveX", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int SetDisableActiveX_32(IntPtr WebShotHandle, int DisableActiveX);
   public static int SetDisableActiveX(IntPtr WebShotHandle, int DisableActiveX)
   {
      if (Environment.Is64BitProcess)
         return SetDisableActiveX_64(WebShotHandle, DisableActiveX);
      else
         return SetDisableActiveX_32(WebShotHandle, DisableActiveX);
   }

   [DllImport("webshot64.dll", EntryPoint = "WebShot_GetDisableScripts", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int GetDisableScripts_64(IntPtr WebShotHandle, ref int DisableScripts);
   [DllImport("webshot.dll", EntryPoint = "WebShot_GetDisableScripts", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int GetDisableScripts_32(IntPtr WebShotHandle, ref int DisableScripts);
   public static int GetDisableScripts(IntPtr WebShotHandle, ref int DisableScripts)
   {
      if (Environment.Is64BitProcess)
         return GetDisableScripts_64(WebShotHandle, ref DisableScripts);
      else
         return GetDisableScripts_32(WebShotHandle, ref DisableScripts);
   }

   [DllImport("webshot64.dll", EntryPoint = "WebShot_SetDisableScripts", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int SetDisableScripts_64(IntPtr WebShotHandle, int DisableScripts);
   [DllImport("webshot.dll", EntryPoint = "WebShot_SetDisableScripts", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int SetDisableScripts_32(IntPtr WebShotHandle, int DisableScripts);
   public static int SetDisableScripts(IntPtr WebShotHandle, int DisableScripts)
   {
      if (Environment.Is64BitProcess)
         return SetDisableScripts_64(WebShotHandle, DisableScripts);
      else
         return SetDisableScripts_32(WebShotHandle, DisableScripts);
   }
   
   [DllImport("webshot64.dll", EntryPoint = "WebShot_GetBrowserWidth", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int GetBrowserWidth_64(IntPtr WebShotHandle, ref int Width);
   [DllImport("webshot.dll", EntryPoint = "WebShot_GetBrowserWidth", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int GetBrowserWidth_32(IntPtr WebShotHandle, ref int Width);
   [DllImport("webshot_svc64.dll", EntryPoint = "WebShot_GetBrowserWidth", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
   private static extern int GetBrowserWidth_svc64(IntPtr WebShotHandle, ref int Width);
   [DllImport("webshot_svc.dll", EntryPoint = "WebShot_GetBrowserWidth", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
   private static extern int GetBrowserWidth_svc32(IntPtr WebShotHandle, ref int Width);
   public static int GetBrowserWidth(IntPtr WebShotHandle, ref int Width)
   {
      var t = GetTable();
      switch (t)
      {
         case TableValue.x32_nosvc:
            return GetBrowserWidth_32(WebShotHandle, ref Width);
         case TableValue.x32_svc:
            return GetBrowserWidth_svc32(WebShotHandle, ref Width);
         case TableValue.x64_nosvc:
            return GetBrowserWidth_64(WebShotHandle, ref Width);
         case TableValue.x64_svc:
            return GetBrowserWidth_svc64(WebShotHandle, ref Width);
      }
      return -1;
   }

   [DllImport("webshot64.dll", EntryPoint = "WebShot_SetBrowserWidth", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int SetBrowserWidth_64(IntPtr WebShotHandle, int Width);
   [DllImport("webshot.dll", EntryPoint = "WebShot_SetBrowserWidth", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int SetBrowserWidth_32(IntPtr WebShotHandle, int Width);
   [DllImport("webshot_svc64.dll", EntryPoint = "WebShot_SetBrowserWidth", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
   private static extern int SetBrowserWidth_svc64(IntPtr WebShotHandle, int Width);
   [DllImport("webshot_svc.dll", EntryPoint = "WebShot_SetBrowserWidth", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
   private static extern int SetBrowserWidth_svc32(IntPtr WebShotHandle, int Width);
   public static int SetBrowserWidth(IntPtr WebShotHandle, int Width)
   {
      var t = GetTable();
      switch (t)
      {
         case TableValue.x32_nosvc:
            return SetBrowserWidth_32(WebShotHandle, Width);
         case TableValue.x32_svc:
            return SetBrowserWidth_svc32(WebShotHandle, Width);
         case TableValue.x64_nosvc:
            return SetBrowserWidth_64(WebShotHandle, Width);
         case TableValue.x64_svc:
            return SetBrowserWidth_svc64(WebShotHandle, Width);
      }
      return -1;
   }

   [DllImport("webshot64.dll", EntryPoint = "WebShot_GetBrowserHeight", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int GetBrowserHeight_64(IntPtr WebShotHandle, ref int Height);
   [DllImport("webshot.dll", EntryPoint = "WebShot_GetBrowserHeight", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int GetBrowserHeight_32(IntPtr WebShotHandle, ref int Height);
   [DllImport("webshot_svc64.dll", EntryPoint = "WebShot_GetBrowserHeight", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
   private static extern int GetBrowserHeight_svc64(IntPtr WebShotHandle, ref int Height);
   [DllImport("webshot_svc.dll", EntryPoint = "WebShot_GetBrowserHeight", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
   private static extern int GetBrowserHeight_svc32(IntPtr WebShotHandle, ref int Height);
   public static int GetBrowserHeight(IntPtr WebShotHandle, ref int Height)
   {
      var t = GetTable();
      switch (t)
      {
         case TableValue.x32_nosvc:
            return GetBrowserHeight_32(WebShotHandle, ref Height);
         case TableValue.x32_svc:
            return GetBrowserHeight_svc32(WebShotHandle, ref Height);
         case TableValue.x64_nosvc:
            return GetBrowserHeight_64(WebShotHandle, ref Height);
         case TableValue.x64_svc:
            return GetBrowserHeight_svc64(WebShotHandle, ref Height);
      }
      return -1;
   }

   [DllImport("webshot64.dll", EntryPoint = "WebShot_SetBrowserHeight", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int SetBrowserHeight_64(IntPtr WebShotHandle, int Height);
   [DllImport("webshot.dll", EntryPoint = "WebShot_SetBrowserHeight", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int SetBrowserHeight_32(IntPtr WebShotHandle, int Height);
   [DllImport("webshot_svc64.dll", EntryPoint = "WebShot_SetBrowserHeight", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
   private static extern int SetBrowserHeight_svc64(IntPtr WebShotHandle, int Height);
   [DllImport("webshot_svc.dll", EntryPoint = "WebShot_SetBrowserHeight", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
   private static extern int SetBrowserHeight_svc32(IntPtr WebShotHandle, int Height);
   public static int SetBrowserHeight(IntPtr WebShotHandle, int Height)
   {
      var t = GetTable();
      switch (t)
      {
         case TableValue.x32_nosvc:
            return SetBrowserHeight_32(WebShotHandle, Height);
         case TableValue.x32_svc:
            return SetBrowserHeight_svc32(WebShotHandle, Height);
         case TableValue.x64_nosvc:
            return SetBrowserHeight_64(WebShotHandle, Height);
         case TableValue.x64_svc:
            return SetBrowserHeight_svc64(WebShotHandle, Height);
      }
      return -1;
   }

   [DllImport("webshot64.dll", EntryPoint = "WebShot_GetBrowserWidthMinimum", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int GetBrowserWidthMinimum_64(IntPtr WebShotHandle, ref int MinimumWidth);
   [DllImport("webshot.dll", EntryPoint = "WebShot_GetBrowserWidthMinimum", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int GetBrowserWidthMinimum_32(IntPtr WebShotHandle, ref int MinimumWidth);
   public static int GetBrowserWidthMinimum(IntPtr WebShotHandle, ref int MinimumWidth)
   {
      if (Environment.Is64BitProcess)
         return GetBrowserWidthMinimum_64(WebShotHandle, ref MinimumWidth);
      else
         return GetBrowserWidthMinimum_32(WebShotHandle, ref MinimumWidth);
   }

   [DllImport("webshot64.dll", EntryPoint = "WebShot_SetBrowserWidthMinimum", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int SetBrowserWidthMinimum_64(IntPtr WebShotHandle, int MinimumWidth);
   [DllImport("webshot.dll", EntryPoint = "WebShot_SetBrowserWidthMinimum", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int SetBrowserWidthMinimum_32(IntPtr WebShotHandle, int MinimumWidth);
   public static int SetBrowserWidthMinimum(IntPtr WebShotHandle, int MinimumWidth)
   {
      if (Environment.Is64BitProcess)
         return SetBrowserWidthMinimum_64(WebShotHandle, MinimumWidth);
      else
         return SetBrowserWidthMinimum_32(WebShotHandle, MinimumWidth);
   }

   [DllImport("webshot64.dll", EntryPoint = "WebShot_GetBrowserHeightMinimum", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int GetBrowserHeightMinimum_64(IntPtr WebShotHandle, ref int MinimumHeight);
   [DllImport("webshot.dll", EntryPoint = "WebShot_GetBrowserHeightMinimum", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int GetBrowserHeightMinimum_32(IntPtr WebShotHandle, ref int MinimumHeight);
   public static int GetBrowserHeightMinimum(IntPtr WebShotHandle, ref int MinimumHeight)
   {
      if (Environment.Is64BitProcess)
         return GetBrowserHeightMinimum_64(WebShotHandle, ref MinimumHeight);
      else
         return GetBrowserHeightMinimum_32(WebShotHandle, ref MinimumHeight);
   }

   [DllImport("webshot64.dll", EntryPoint = "WebShot_SetBrowserHeightMinimum", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int SetBrowserHeightMinimum_64(IntPtr WebShotHandle, int MinimumHeight);
   [DllImport("webshot.dll", EntryPoint = "WebShot_SetBrowserHeightMinimum", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int SetBrowserHeightMinimum_32(IntPtr WebShotHandle, int MinimumHeight);
   public static int SetBrowserHeightMinimum(IntPtr WebShotHandle, int MinimumHeight)
   {
      if (Environment.Is64BitProcess)
         return SetBrowserHeightMinimum_64(WebShotHandle, MinimumHeight);
      else
         return SetBrowserHeightMinimum_32(WebShotHandle, MinimumHeight);
   }

   [DllImport("webshot64.dll", EntryPoint = "WebShot_GetBrowserWidthMaximum", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int GetBrowserWidthMaximum_64(IntPtr WebShotHandle, ref int MaximumWidth);
   [DllImport("webshot.dll", EntryPoint = "WebShot_GetBrowserWidthMaximum", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int GetBrowserWidthMaximum_32(IntPtr WebShotHandle, ref int MaximumWidth);
   public static int GetBrowserWidthMaximum(IntPtr WebShotHandle, ref int MaximumWidth)
   {
      if (Environment.Is64BitProcess)
         return GetBrowserWidthMaximum_64(WebShotHandle, ref MaximumWidth);
      else
         return GetBrowserWidthMaximum_32(WebShotHandle, ref MaximumWidth);
   }
   
   [DllImport("webshot64.dll", EntryPoint = "WebShot_SetBrowserWidthMaximum", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int SetBrowserWidthMaximum_64(IntPtr WebShotHandle, int MaximumWidth);
   [DllImport("webshot.dll", EntryPoint = "WebShot_SetBrowserWidthMaximum", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int SetBrowserWidthMaximum_32(IntPtr WebShotHandle, int MaximumWidth);
   public static int SetBrowserWidthMaximum(IntPtr WebShotHandle, int MaximumWidth)
   {
      if (Environment.Is64BitProcess)
         return SetBrowserWidthMaximum_64(WebShotHandle, MaximumWidth);
      else
         return SetBrowserWidthMaximum_32(WebShotHandle, MaximumWidth);
   }
   
   [DllImport("webshot64.dll", EntryPoint = "WebShot_GetBrowserHeightMaximum", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int GetBrowserHeightMaximum_64(IntPtr WebShotHandle, ref int MaximumHeight);
   [DllImport("webshot.dll", EntryPoint = "WebShot_GetBrowserHeightMaximum", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int GetBrowserHeightMaximum_32(IntPtr WebShotHandle, ref int MaximumHeight);
   [DllImport("webshot_svc64.dll", EntryPoint = "WebShot_GetBrowserHeightMaximum", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
   private static extern int GetBrowserHeightMaximum_svc64(IntPtr WebShotHandle, ref int MaximumHeight);
   [DllImport("webshot_svc.dll", EntryPoint = "WebShot_GetBrowserHeightMaximum", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
   private static extern int GetBrowserHeightMaximum_svc32(IntPtr WebShotHandle, ref int MaximumHeight);
   public static int GetBrowserHeightMaximum(IntPtr WebShotHandle, ref int MaximumHeight)
   {
      var t = GetTable();
      switch (t)
      {
         case TableValue.x32_nosvc:
            return GetBrowserHeightMaximum_32(WebShotHandle, ref MaximumHeight);
         case TableValue.x32_svc:
            return GetBrowserHeightMaximum_svc32(WebShotHandle, ref MaximumHeight);
         case TableValue.x64_nosvc:
            return GetBrowserHeightMaximum_64(WebShotHandle, ref MaximumHeight);
         case TableValue.x64_svc:
            return GetBrowserHeightMaximum_svc64(WebShotHandle, ref MaximumHeight);
      }
      return -1;
   }

   [DllImport("webshot64.dll", EntryPoint = "WebShot_SetBrowserHeightMaximum", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int SetBrowserHeightMaximum_64(IntPtr WebShotHandle, int MaximumHeight);
   [DllImport("webshot.dll", EntryPoint = "WebShot_SetBrowserHeightMaximum", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int SetBrowserHeightMaximum_32(IntPtr WebShotHandle, int MaximumHeight);
   [DllImport("webshot_svc64.dll", EntryPoint = "WebShot_SetBrowserHeightMaximum", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
   private static extern int SetBrowserHeightMaximum_svc64(IntPtr WebShotHandle, int MaximumHeight);
   [DllImport("webshot_svc.dll", EntryPoint = "WebShot_SetBrowserHeightMaximum", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
   private static extern int SetBrowserHeightMaximum_svc32(IntPtr WebShotHandle, int MaximumHeight);
   public static int SetBrowserHeightMaximum(IntPtr WebShotHandle, int MaximumHeight)
   {
      var t = GetTable();
      switch (t)
      {
         case TableValue.x32_nosvc:
            return SetBrowserHeightMaximum_32(WebShotHandle, MaximumHeight);
         case TableValue.x32_svc:
            return SetBrowserHeightMaximum_svc32(WebShotHandle, MaximumHeight);
         case TableValue.x64_nosvc:
            return SetBrowserHeightMaximum_64(WebShotHandle, MaximumHeight);
         case TableValue.x64_svc:
            return SetBrowserHeightMaximum_svc64(WebShotHandle, MaximumHeight);
      }
      return -1;
   }

   [DllImport("webshot64.dll", EntryPoint = "WebShot_GetHttpCode", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int GetHttpCode_64(IntPtr WebShotHandle, ref int HttpCode);
   [DllImport("webshot.dll", EntryPoint = "WebShot_GetHttpCode", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int GetHttpCode_32(IntPtr WebShotHandle, ref int HttpCode);
   [DllImport("webshot_svc64.dll", EntryPoint = "WebShot_GetHttpCode", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
   private static extern int GetHttpCode_svc64(IntPtr WebShotHandle, ref int HttpCode);
   [DllImport("webshot_svc.dll", EntryPoint = "WebShot_GetHttpCode", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
   private static extern int GetHttpCode_svc32(IntPtr WebShotHandle, ref int HttpCode);
   public static int GetHttpCode(IntPtr WebShotHandle, ref int HttpCode)
   {
      var t = GetTable();
      switch (t)
      {
         case TableValue.x32_nosvc:
            return GetHttpCode_32(WebShotHandle, ref HttpCode);
         case TableValue.x32_svc:
            return GetHttpCode_svc32(WebShotHandle, ref HttpCode);
         case TableValue.x64_nosvc:
            return GetHttpCode_64(WebShotHandle, ref HttpCode);
         case TableValue.x64_svc:
            return GetHttpCode_svc64(WebShotHandle, ref HttpCode);
      }
      return -1;
   }

   [DllImport("webshot64.dll", EntryPoint = "WebShot_GetBrowserVisible", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int GetBrowserVisible_64(IntPtr WebShotHandle, ref int Visible);
   [DllImport("webshot.dll", EntryPoint = "WebShot_GetBrowserVisible", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int GetBrowserVisible_32(IntPtr WebShotHandle, ref int Visible);
   public static int GetBrowserVisible(IntPtr WebShotHandle, ref int Visible)
   {
      if (Environment.Is64BitProcess)
         return GetBrowserVisible_64(WebShotHandle, ref Visible);
      else
         return GetBrowserVisible_32(WebShotHandle, ref Visible);
   }

   [DllImport("webshot64.dll", EntryPoint = "WebShot_SetBrowserVisible", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int SetBrowserVisible_64(IntPtr WebShotHandle, int Visible);
   [DllImport("webshot.dll", EntryPoint = "WebShot_SetBrowserVisible", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int SetBrowserVisible_32(IntPtr WebShotHandle, int Visible);
   public static int SetBrowserVisible(IntPtr WebShotHandle, int Visible)
   {
      if (Environment.Is64BitProcess)
         return SetBrowserVisible_64(WebShotHandle, Visible);
      else
         return SetBrowserVisible_32(WebShotHandle, Visible);
   }


   [DllImport("webshot64.dll", EntryPoint = "WebShot_GetImageFilename", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int GetImageFilename_64(IntPtr WebShotHandle, StringBuilder ImageFilename, int MaxImageFilename);
   [DllImport("webshot.dll", EntryPoint = "WebShot_GetImageFilename", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int GetImageFilename_32(IntPtr WebShotHandle, StringBuilder ImageFilename, int MaxImageFilename);
   public static int GetImageFilename(IntPtr WebShotHandle, StringBuilder ImageFilename, int MaxImageFilename)
   {
      if (Environment.Is64BitProcess)
         return GetImageFilename_64(WebShotHandle, ImageFilename, MaxImageFilename);
      else
         return GetImageFilename_32(WebShotHandle, ImageFilename, MaxImageFilename);
   }

   [DllImport("webshot64.dll", EntryPoint = "WebShot_GetImageType", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int GetImageType_64(IntPtr WebShotHandle, StringBuilder Type, int MaxType);
   [DllImport("webshot.dll", EntryPoint = "WebShot_GetImageType", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int GetImageType_32(IntPtr WebShotHandle, StringBuilder Type, int MaxType);
   public static int GetImageType(IntPtr WebShotHandle, StringBuilder Type, int MaxType)
   {
      if (Environment.Is64BitProcess)
         return GetImageType_64(WebShotHandle, Type, MaxType);
      else
         return GetImageType_32(WebShotHandle, Type, MaxType);
   }

   [DllImport("webshot64.dll", EntryPoint = "WebShot_SetImageType", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int SetImageType_64(IntPtr WebShotHandle, string Type);
   [DllImport("webshot.dll", EntryPoint = "WebShot_SetImageType", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int SetImageType_32(IntPtr WebShotHandle, string Type);
   public static int SetImageType(IntPtr WebShotHandle, string Type)
   {
      if (Environment.Is64BitProcess)
         return SetImageType_64(WebShotHandle, Type);
      else
         return SetImageType_32(WebShotHandle, Type);
   }
   
   [DllImport("webshot64.dll", EntryPoint = "WebShot_GetImageWidth", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int GetImageWidth_64(IntPtr WebShotHandle, ref int Width);
   [DllImport("webshot.dll", EntryPoint = "WebShot_GetImageWidth", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int GetImageWidth_32(IntPtr WebShotHandle, ref int Width);
   public static int GetImageWidth(IntPtr WebShotHandle, ref int Width)
   {
      if (Environment.Is64BitProcess)
         return GetImageWidth_64(WebShotHandle, ref Width);
      else
         return GetImageWidth_32(WebShotHandle, ref Width);
   }
   
   [DllImport("webshot64.dll", EntryPoint = "WebShot_SetImageWidth", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int SetImageWidth_64(IntPtr WebShotHandle, int Width);
   [DllImport("webshot.dll", EntryPoint = "WebShot_SetImageWidth", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int SetImageWidth_32(IntPtr WebShotHandle, int Width);
   public static int SetImageWidth(IntPtr WebShotHandle, int Width)
   {
      if (Environment.Is64BitProcess)
         return SetImageWidth_64(WebShotHandle, Width);
      else
         return SetImageWidth_32(WebShotHandle, Width);
   }

   [DllImport("webshot64.dll", EntryPoint = "WebShot_GetImageHeight", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int GetImageHeight_64(IntPtr WebShotHandle, ref int Height);
   [DllImport("webshot.dll", EntryPoint = "WebShot_GetImageHeight", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int GetImageHeight_32(IntPtr WebShotHandle, ref int Height);
   public static int GetImageHeight(IntPtr WebShotHandle, ref int Height)
   {
      if (Environment.Is64BitProcess)
         return GetImageHeight_64(WebShotHandle, ref Height);
      else
         return GetImageHeight_32(WebShotHandle, ref Height);
   }

   [DllImport("webshot64.dll", EntryPoint = "WebShot_SetImageHeight", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int SetImageHeight_64(IntPtr WebShotHandle, int Height);
   [DllImport("webshot.dll", EntryPoint = "WebShot_SetImageHeight", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int SetImageHeight_32(IntPtr WebShotHandle, int Height);
   public static int SetImageHeight(IntPtr WebShotHandle, int Height)
   {
      if (Environment.Is64BitProcess)
         return SetImageHeight_64(WebShotHandle, Height);
      else
         return SetImageHeight_32(WebShotHandle, Height);
   }

   [DllImport("webshot64.dll", EntryPoint = "WebShot_GetImageWidthMaximum", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int GetImageWidthMaximum_64(IntPtr WebShotHandle, ref int Width);
   [DllImport("webshot.dll", EntryPoint = "WebShot_GetImageWidthMaximum", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int GetImageWidthMaximum_32(IntPtr WebShotHandle, ref int Width);
   public static int GetImageWidthMaximum(IntPtr WebShotHandle, ref int Width)
   {
      if (Environment.Is64BitProcess)
         return GetImageWidthMaximum_64(WebShotHandle, ref Width);
      else
         return GetImageWidthMaximum_32(WebShotHandle, ref Width);
   }

   [DllImport("webshot64.dll", EntryPoint = "WebShot_SetImageWidthMaximum", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int SetImageWidthMaximum_64(IntPtr WebShotHandle, int Width);
   [DllImport("webshot.dll", EntryPoint = "WebShot_SetImageWidthMaximum", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int SetImageWidthMaximum_32(IntPtr WebShotHandle, int Width);
   public static int SetImageWidthMaximum(IntPtr WebShotHandle, int Width)
   {
      if (Environment.Is64BitProcess)
         return SetImageWidthMaximum_64(WebShotHandle, Width);
      else
         return SetImageWidthMaximum_32(WebShotHandle, Width);
   }

   [DllImport("webshot64.dll", EntryPoint = "WebShot_GetImageHeightMaximum", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int GetImageHeightMaximum_64(IntPtr WebShotHandle, ref int Height);
   [DllImport("webshot.dll", EntryPoint = "WebShot_GetImageHeightMaximum", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int GetImageHeightMaximum_32(IntPtr WebShotHandle, ref int Height);
   public static int GetImageHeightMaximum(IntPtr WebShotHandle, ref int Height)
   {
      if (Environment.Is64BitProcess)
         return GetImageHeightMaximum_64(WebShotHandle, ref Height);
      else
         return GetImageHeightMaximum_32(WebShotHandle, ref Height);
   }
   
   [DllImport("webshot64.dll", EntryPoint = "WebShot_SetImageHeightMaximum", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int SetImageHeightMaximum_64(IntPtr WebShotHandle, int Height);
   [DllImport("webshot.dll", EntryPoint = "WebShot_SetImageHeightMaximum", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int SetImageHeightMaximum_32(IntPtr WebShotHandle, int Height);
   public static int SetImageHeightMaximum(IntPtr WebShotHandle, int Height)
   {
      if (Environment.Is64BitProcess)
         return SetImageHeightMaximum_64(WebShotHandle, Height);
      else
         return SetImageHeightMaximum_32(WebShotHandle, Height);
   }

   [DllImport("webshot64.dll", EntryPoint = "WebShot_GetImageQuality", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int GetImageQuality_64(IntPtr WebShotHandle, ref int ImageQuality);
   [DllImport("webshot.dll", EntryPoint = "WebShot_GetImageQuality", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int GetImageQuality_32(IntPtr WebShotHandle, ref int ImageQuality);
   public static int GetImageQuality(IntPtr WebShotHandle, ref int ImageQuality)
   {
      if (Environment.Is64BitProcess)
         return GetImageQuality_64(WebShotHandle, ref ImageQuality);
      else
         return GetImageQuality_32(WebShotHandle, ref ImageQuality);
   }

   [DllImport("webshot64.dll", EntryPoint = "WebShot_SetImageQuality", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int SetImageQuality_64(IntPtr WebShotHandle, int ImageQuality);
   [DllImport("webshot.dll", EntryPoint = "WebShot_SetImageQuality", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int SetImageQuality_32(IntPtr WebShotHandle, int ImageQuality);
   public static int SetImageQuality(IntPtr WebShotHandle, int ImageQuality)
   {
      if (Environment.Is64BitProcess)
         return SetImageQuality_64(WebShotHandle, ImageQuality);
      else
         return SetImageQuality_32(WebShotHandle, ImageQuality);
   }
   
   [DllImport("webshot64.dll", EntryPoint = "WebShot_GetImageGrayscale", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int GetImageGrayscale_64(IntPtr WebShotHandle, ref int ImageGrayscale);
   [DllImport("webshot.dll", EntryPoint = "WebShot_GetImageGrayscale", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int GetImageGrayscale_32(IntPtr WebShotHandle, ref int ImageGrayscale);
   public static int GetImageGrayscale(IntPtr WebShotHandle, ref int ImageGrayscale)
   {
      if (Environment.Is64BitProcess)
         return GetImageGrayscale_64(WebShotHandle, ref ImageGrayscale);
      else
         return GetImageGrayscale_32(WebShotHandle, ref ImageGrayscale);
   }

   [DllImport("webshot64.dll", EntryPoint = "WebShot_SetImageGrayscale", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int SetImageGrayscale_64(IntPtr WebShotHandle, int ImageGrayscale);
   [DllImport("webshot.dll", EntryPoint = "WebShot_SetImageGrayscale", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int SetImageGrayscale_32(IntPtr WebShotHandle, int ImageGrayscale);
   public static int SetImageGrayscale(IntPtr WebShotHandle, int ImageGrayscale)
   {
      if (Environment.Is64BitProcess)
         return SetImageGrayscale_64(WebShotHandle, ImageGrayscale);
      else
         return SetImageGrayscale_32(WebShotHandle, ImageGrayscale);
   }
   [DllImport("webshot64.dll", EntryPoint = "WebShot_GetImageSaveToDisk", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int GetImageSaveToDisk_64(IntPtr WebShotHandle, ref int SaveToDisk);
   [DllImport("webshot.dll", EntryPoint = "WebShot_GetImageSaveToDisk", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int GetImageSaveToDisk_32(IntPtr WebShotHandle, ref int SaveToDisk);
   public static int GetImageSaveToDisk(IntPtr WebShotHandle, ref int SaveToDisk)
   {
      if (Environment.Is64BitProcess)
         return GetImageSaveToDisk_64(WebShotHandle, ref SaveToDisk);
      else
         return GetImageSaveToDisk_32(WebShotHandle, ref SaveToDisk);
   }

   [DllImport("webshot64.dll", EntryPoint = "WebShot_SetImageSaveToDisk", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int SetImageSaveToDisk_64(IntPtr WebShotHandle, int SaveToDisk);
   [DllImport("webshot.dll", EntryPoint = "WebShot_SetImageSaveToDisk", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int SetImageSaveToDisk_32(IntPtr WebShotHandle, int SaveToDisk);
   public static int SetImageSaveToDisk(IntPtr WebShotHandle, int SaveToDisk)
   {
      if (Environment.Is64BitProcess)
         return SetImageSaveToDisk_64(WebShotHandle, SaveToDisk);
      else
         return SetImageSaveToDisk_32(WebShotHandle, SaveToDisk);
   }

   [DllImport("webshot64.dll", EntryPoint = "WebShot_SetImageCrop", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int SetImageCrop_64(IntPtr WebShotHandle, int Left, int Top, int Right, int Bottom);
   [DllImport("webshot.dll", EntryPoint = "WebShot_SetImageCrop", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int SetImageCrop_32(IntPtr WebShotHandle, int Left, int Top, int Right, int Bottom);
   public static int SetImageCrop(IntPtr WebShotHandle, int Left, int Top, int Right, int Bottom)
   {
      if (Environment.Is64BitProcess)
         return SetImageCrop_64(WebShotHandle, Left, Top, Right, Bottom);
      else
         return SetImageCrop_32(WebShotHandle, Left, Top, Right, Bottom);
   }

   [DllImport("webshot64.dll", EntryPoint = "WebShot_SetWatermarkFilename", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int SetWatermarkFilename_64(IntPtr WebShotHandle, string Filename);
   [DllImport("webshot.dll", EntryPoint = "WebShot_SetWatermarkFilename", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int SetWatermarkFilename_32(IntPtr WebShotHandle, string Filename);
   public static int SetWatermarkFilename(IntPtr WebShotHandle, string Filename)
   {
      if (Environment.Is64BitProcess)
         return SetWatermarkFilename_64(WebShotHandle, Filename);
      else
         return SetWatermarkFilename_32(WebShotHandle, Filename);
   }

   [DllImport("webshot64.dll", EntryPoint = "WebShot_SetWatermarkPosition", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int SetWatermarkPosition_64(IntPtr WebShotHandle, float MultiX, float MultiY);
   [DllImport("webshot.dll", EntryPoint = "WebShot_SetWatermarkPosition", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int SetWatermarkPosition_32(IntPtr WebShotHandle, float MultiX, float MultiY);
   public static int SetWatermarkPosition(IntPtr WebShotHandle, float MultiX, float MultiY)
   {
      if (Environment.Is64BitProcess)
         return SetWatermarkPosition_64(WebShotHandle, MultiX, MultiY);
      else
         return SetWatermarkPosition_32(WebShotHandle, MultiX, MultiY);
   }

   [DllImport("webshot64.dll", EntryPoint = "WebShot_SetWatermarkOpacity", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int SetWatermarkOpacity_64(IntPtr WebShotHandle, float Opacity);
   [DllImport("webshot.dll", EntryPoint = "WebShot_SetWatermarkOpacity", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int SetWatermarkOpacity_32(IntPtr WebShotHandle, float Opacity);
   public static int SetWatermarkOpacity(IntPtr WebShotHandle, float Opacity)
   {
      if (Environment.Is64BitProcess)
         return SetWatermarkOpacity_64(WebShotHandle, Opacity);
      else
         return SetWatermarkOpacity_32(WebShotHandle, Opacity);
   }

   [DllImport("webshot64.dll", EntryPoint = "WebShot_GetCsvFilename", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int GetCsvFilename_64(IntPtr WebShotHandle, StringBuilder Filename, int MaxFilename);
   [DllImport("webshot.dll", EntryPoint = "WebShot_GetCsvFilename", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int GetCsvFilename_32(IntPtr WebShotHandle, StringBuilder Filename, int MaxFilename);
   public static int GetCsvFilename(IntPtr WebShotHandle, StringBuilder Filename, int MaxFilename)
   {
      if (Environment.Is64BitProcess)
         return GetCsvFilename_64(WebShotHandle, Filename, MaxFilename);
      else
         return GetCsvFilename_32(WebShotHandle, Filename, MaxFilename);
   }

   [DllImport("webshot64.dll", EntryPoint = "WebShot_SetCsvFilename", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int SetCsvFilename_64(IntPtr WebShotHandle, string Filename);
   [DllImport("webshot.dll", EntryPoint = "WebShot_SetCsvFilename", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int SetCsvFilename_32(IntPtr WebShotHandle, string Filename);
   public static int SetCsvFilename(IntPtr WebShotHandle, string Filename)
   {
      if (Environment.Is64BitProcess)
         return SetCsvFilename_64(WebShotHandle, Filename);
      else
         return SetCsvFilename_32(WebShotHandle, Filename);
   }

   [DllImport("webshot64.dll", EntryPoint = "WebShot_GetHtmlFilename", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int GetHtmlFilename_64(IntPtr WebShotHandle, StringBuilder Filename, int MaxFilename);
   [DllImport("webshot.dll", EntryPoint = "WebShot_GetHtmlFilename", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int GetHtmlFilename_32(IntPtr WebShotHandle, StringBuilder Filename, int MaxFilename);
   [DllImport("webshot_svc64.dll", EntryPoint = "WebShot_GetHtmlFilename", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
   private static extern int GetHtmlFilename_svc64(IntPtr WebShotHandle, StringBuilder Filename, int MaxFilename);
   [DllImport("webshot_svc.dll", EntryPoint = "WebShot_GetHtmlFilename", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
   private static extern int GetHtmlFilename_svc32(IntPtr WebShotHandle, StringBuilder Filename, int MaxFilename);
   public static int GetHtmlFilename(IntPtr WebShotHandle, StringBuilder Filename, int MaxFilename)
   {
      var t = GetTable();
      switch (t)
      {
         case TableValue.x32_nosvc:
            return GetHtmlFilename_32(WebShotHandle, Filename, MaxFilename);
         case TableValue.x32_svc:
            return GetHtmlFilename_svc32(WebShotHandle, Filename, MaxFilename);
         case TableValue.x64_nosvc:
            return GetHtmlFilename_64(WebShotHandle, Filename, MaxFilename);
         case TableValue.x64_svc:
            return GetHtmlFilename_svc64(WebShotHandle, Filename, MaxFilename);
      }
      return -1;
   }

   [DllImport("webshot64.dll", EntryPoint = "WebShot_SetHtmlFilename", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int SetHtmlFilename_64(IntPtr WebShotHandle, string Filename);
   [DllImport("webshot.dll", EntryPoint = "WebShot_SetHtmlFilename", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int SetHtmlFilename_32(IntPtr WebShotHandle, string Filename);
   [DllImport("webshot_svc64.dll", EntryPoint = "WebShot_SetHtmlFilename", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
   private static extern int SetHtmlFilename_svc64(IntPtr WebShotHandle, string Filename);
   [DllImport("webshot_svc.dll", EntryPoint = "WebShot_SetHtmlFilename", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
   private static extern int SetHtmlFilename_svc32(IntPtr WebShotHandle, string Filename);
   public static int SetHtmlFilename(IntPtr WebShotHandle, string Filename)
   {
      var t = GetTable();
      switch (t)
      {
         case TableValue.x32_nosvc:
            return SetHtmlFilename_32(WebShotHandle, Filename);
         case TableValue.x32_svc:
            return SetHtmlFilename_svc32(WebShotHandle, Filename);
         case TableValue.x64_nosvc:
            return SetHtmlFilename_64(WebShotHandle, Filename);
         case TableValue.x64_svc:
            return SetHtmlFilename_svc64(WebShotHandle, Filename);
      }
      return -1;
   }

   [DllImport("webshot64.dll", EntryPoint = "WebShot_GetLinkFilename", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int GetLinkFilename_64(IntPtr WebShotHandle, StringBuilder Filename, int MaxFilename);
   [DllImport("webshot.dll", EntryPoint = "WebShot_GetLinkFilename", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int GetLinkFilename_32(IntPtr WebShotHandle, StringBuilder Filename, int MaxFilename);
   public static int GetLinkFilename(IntPtr WebShotHandle, StringBuilder Filename, int MaxFilename)
   {
      if (Environment.Is64BitProcess)
         return GetLinkFilename_64(WebShotHandle, Filename, MaxFilename);
      else
         return GetLinkFilename_32(WebShotHandle, Filename, MaxFilename);
   }

   [DllImport("webshot64.dll", EntryPoint = "WebShot_SetLinkFilename", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int SetLinkFilename_64(IntPtr WebShotHandle, string Filename);
   [DllImport("webshot.dll", EntryPoint = "WebShot_SetLinkFilename", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int SetLinkFilename_32(IntPtr WebShotHandle, string Filename);
   public static int SetLinkFilename(IntPtr WebShotHandle, string Filename)
   {
      if (Environment.Is64BitProcess)
         return SetLinkFilename_64(WebShotHandle, Filename);
      else
         return SetLinkFilename_32(WebShotHandle, Filename);
   }
   
   [DllImport("webshot64.dll", EntryPoint = "WebShot_GetError", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int GetError_64(IntPtr WebShotHandle, StringBuilder Error, int MaxError);
   [DllImport("webshot.dll", EntryPoint = "WebShot_GetError", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int GetError_32(IntPtr WebShotHandle, StringBuilder Error, int MaxError);
   [DllImport("webshot_svc64.dll", EntryPoint = "WebShot_GetError", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
   private static extern int GetError_svc64(IntPtr WebShotHandle, StringBuilder Error, int MaxError);
   [DllImport("webshot_svc.dll", EntryPoint = "WebShot_GetError", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
   private static extern int GetError_svc32(IntPtr WebShotHandle, StringBuilder Error, int MaxError);
   public static int GetError(IntPtr WebShotHandle, StringBuilder Error, int MaxError)
   {
      var t = GetTable();
      switch (t)
      {
         case TableValue.x32_nosvc:
            return GetError_32(WebShotHandle, Error, MaxError);
         case TableValue.x32_svc:
            return GetError_svc32(WebShotHandle, Error, MaxError);
         case TableValue.x64_nosvc:
            return GetError_64(WebShotHandle, Error, MaxError);
         case TableValue.x64_svc:
            return GetError_svc64(WebShotHandle, Error, MaxError);
      }
      return -1;
   }

   [DllImport("webshot64.dll", EntryPoint = "WebShot_GetVerbose", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int GetVerbose_64(IntPtr WebShotHandle, ref int Verbose);
   [DllImport("webshot.dll", EntryPoint = "WebShot_GetVerbose", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int GetVerbose_32(IntPtr WebShotHandle, ref int Verbose);
   [DllImport("webshot_svc64.dll", EntryPoint = "WebShot_GetVerbose", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
   private static extern int GetVerbose_svc64(IntPtr WebShotHandle, ref int Verbose);
   [DllImport("webshot_svc.dll", EntryPoint = "WebShot_GetVerbose", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
   private static extern int GetVerbose_svc32(IntPtr WebShotHandle, ref int Verbose);
   public static int GetVerbose(IntPtr WebShotHandle, ref int Verbose)
   {
      var t = GetTable();
      switch (t)
      {
         case TableValue.x32_nosvc:
            return GetVerbose_32(WebShotHandle, ref Verbose);
         case TableValue.x32_svc:
            return GetVerbose_svc32(WebShotHandle, ref Verbose);
         case TableValue.x64_nosvc:
            return GetVerbose_64(WebShotHandle, ref Verbose);
         case TableValue.x64_svc:
            return GetVerbose_svc64(WebShotHandle, ref Verbose);
      }
      return -1;
   }

   [DllImport("webshot64.dll", EntryPoint = "WebShot_SetVerbose", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int SetVerbose_64(IntPtr WebShotHandle, int Verbose);
   [DllImport("webshot.dll", EntryPoint = "WebShot_SetVerbose", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int SetVerbose_32(IntPtr WebShotHandle, int Verbose);
   [DllImport("webshot_svc64.dll", EntryPoint = "WebShot_SetVerbose", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
   private static extern int SetVerbose_svc64(IntPtr WebShotHandle, int Verbose);
   [DllImport("webshot_svc.dll", EntryPoint = "WebShot_SetVerbose", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
   private static extern int SetVerbose_svc32(IntPtr WebShotHandle, int Verbose);
   public static int SetVerbose(IntPtr WebShotHandle, int Verbose)
   {
      var t = GetTable();
      switch (t)
      {
         case TableValue.x32_nosvc:
            return SetVerbose_32(WebShotHandle, Verbose);
         case TableValue.x32_svc:
            return SetVerbose_svc32(WebShotHandle, Verbose);
         case TableValue.x64_nosvc:
            return SetVerbose_64(WebShotHandle, Verbose);
         case TableValue.x64_svc:
            return SetVerbose_svc64(WebShotHandle, Verbose);
      }
      return -1;
   }

   [DllImport("webshot64.dll", EntryPoint = "WebShot_GetWindow", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int GetWindow_64(IntPtr WebShotHandle, ref int hWnd);
   [DllImport("webshot.dll", EntryPoint = "WebShot_GetWindow", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int GetWindow_32(IntPtr WebShotHandle, ref int hWnd);
   public static int GetWindow(IntPtr WebShotHandle, ref int hWnd)
   {
      if (Environment.Is64BitProcess)
         return GetWindow_64(WebShotHandle, ref hWnd);
      else
         return GetWindow_32(WebShotHandle, ref hWnd);
   }

   [DllImport("webshot64.dll", EntryPoint = "WebShot_Create", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int Create_64(ref IntPtr WebShotHandle);
   [DllImport("webshot.dll", EntryPoint = "WebShot_Create", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int Create_32(ref IntPtr WebShotHandle);
   [DllImport("webshot_svc64.dll", EntryPoint = "WebShot_Create", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
   private static extern int Create_svc64(ref IntPtr WebShotHandle);
   [DllImport("webshot_svc.dll", EntryPoint = "WebShot_Create", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
   private static extern int Create_svc32(ref IntPtr WebShotHandle);
   public static int Create(ref IntPtr WebShotHandle)
   {
      var t = GetTable();
      switch (t)
      {
         case TableValue.x32_nosvc:
            return Create_32(ref WebShotHandle);
         case TableValue.x32_svc:
            return Create_svc32(ref WebShotHandle);
         case TableValue.x64_nosvc:
            return Create_64(ref WebShotHandle);
         case TableValue.x64_svc:
            return Create_svc64(ref WebShotHandle);
      }
      return -1;
   }

   [DllImport("webshot64.dll", EntryPoint = "WebShot_Destroy", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int Destroy_64(ref IntPtr WebShotHandle);
   [DllImport("webshot.dll", EntryPoint = "WebShot_Destroy", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int Destroy_32(ref IntPtr WebShotHandle);
   [DllImport("webshot_svc64.dll", EntryPoint = "WebShot_Destroy", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
   private static extern int Destroy_svc64(ref IntPtr WebShotHandle);
   [DllImport("webshot_svc.dll", EntryPoint = "WebShot_Destroy", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
   private static extern int Destroy_svc32(ref IntPtr WebShotHandle);
   public static int Destroy(ref IntPtr WebShotHandle)
   {
      var t = GetTable();
      switch (t)
      {
         case TableValue.x32_nosvc:
            return Destroy_32(ref WebShotHandle);
         case TableValue.x32_svc:
            return Destroy_svc32(ref WebShotHandle);
         case TableValue.x64_nosvc:
            return Destroy_64(ref WebShotHandle);
         case TableValue.x64_svc:
            return Destroy_svc64(ref WebShotHandle);
      }
      return -1;
   }

   [DllImport("webshot64.dll", EntryPoint = "WebShot_DllInit", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int DllInit_64(string DebugFilename, int DebugFlags);
   [DllImport("webshot.dll", EntryPoint = "WebShot_DllInit", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int DllInit_32(string DebugFilename, int DebugFlags);
   [DllImport("webshot_svc64.dll", EntryPoint = "WebShot_DllInit", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
   private static extern int DllInit_svc64(string DebugFilename, int DebugFlags);
   [DllImport("webshot_svc.dll", EntryPoint = "WebShot_DllInit", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
   private static extern int DllInit_svc32(string DebugFilename, int DebugFlags);
   public static int DllInit(string DebugFilename, int DebugFlags)
   {
      var t = GetTable();
      switch (t)
      {
         case TableValue.x32_nosvc:
            return DllInit_32(DebugFilename, DebugFlags);
         case TableValue.x32_svc:
            return DllInit_svc32(DebugFilename, DebugFlags);
         case TableValue.x64_nosvc:
            return DllInit_64(DebugFilename, DebugFlags);
         case TableValue.x64_svc:
            return DllInit_svc64(DebugFilename, DebugFlags);
      }
      return -1;
   }

   [DllImport("webshot64.dll", EntryPoint = "WebShot_DllUninit", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int DllUninit_64();
   [DllImport("webshot.dll", EntryPoint = "WebShot_DllUninit", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
   private static extern int DllUninit_32();
   [DllImport("webshot_svc64.dll", EntryPoint = "WebShot_DllUninit", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
   private static extern int DllUninit_svc64();
   [DllImport("webshot_svc.dll", EntryPoint = "WebShot_DllUninit", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
   private static extern int DllUninit_svc32();
   public static int DllUninit()
   {
      var t = GetTable();
      switch (t)
      {
         case TableValue.x32_nosvc:
            return DllUninit_32();
         case TableValue.x32_svc:
            return DllUninit_svc32();
         case TableValue.x64_nosvc:
            return DllUninit_64();
         case TableValue.x64_svc:
            return DllUninit_svc64();
      }
      return -1;
   }
   /*********************************************************************/

}
