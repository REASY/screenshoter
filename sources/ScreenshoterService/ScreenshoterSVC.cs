﻿using Screenshoter;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;

namespace ScreenshoterService
{
   public partial class ScreenshoterSVC : ServiceBase
   {
      public const string EVENT_SOURCE_NAME = "ScreenshoterSVC";
      public const string LOG_NAME = "ScreenshoterSVC";
      public const string CONFIG_FILE_NAME = "ScreenshoterSVC.config";
      private Server _server = null;

      public ScreenshoterSVC()
      {
         InitializeComponent();
      }

      protected override void OnStart(string[] args)
      {
         string exePath = System.Reflection.Assembly.GetEntryAssembly().Location;
         var folderPath = Path.GetDirectoryName(exePath);
         Directory.SetCurrentDirectory(folderPath);
         Settings settings = null;
         try
         {
            settings = Settings.Parse(CONFIG_FILE_NAME);
         }
         catch(Exception ex)
         {
            string msg = string.Format("Can't parse configuration file '{0}': {1}", CONFIG_FILE_NAME, ex.Message);
            ErrorLog(msg);
            throw new Exception(msg, ex);
         }         
         try
         {
            _server = new Server(settings, true);
            _server.Start();
         }
         catch(Exception ex)
         {
            string msg = string.Format("Can't start server: {0}", ex.Message);
            ErrorLog(msg);
            throw new Exception(msg, ex);
         }
         InfoLog("Service started");
      }

      protected override void OnStop()
      {
         try { _server.Stop(); }
         catch { }
         InfoLog("Service stopped.");
      }
      public void InfoLog(string log, params object[] obj)
      {
         bool sourceExist = IsSourceExist();
         try
         {
            if (!sourceExist)
            {
               EventLog.CreateEventSource(EVENT_SOURCE_NAME, LOG_NAME);
            }
            _logger.Source = EVENT_SOURCE_NAME;
            string msg = string.Format(log, obj);
            _logger.WriteEntry(msg, EventLogEntryType.Information);
         }
         catch { }
      }
      public void ErrorLog(string log, params object[] obj)
      {
         bool sourceExist = IsSourceExist();
         try
         {
            if (!sourceExist)
            {
               EventLog.CreateEventSource(EVENT_SOURCE_NAME, LOG_NAME);
            }
            _logger.Source = EVENT_SOURCE_NAME;
            string msg = string.Format(log, obj);
            _logger.WriteEntry(msg, EventLogEntryType.Error);
         }
         catch { }
      }

      private bool IsSourceExist()
      {
         bool sourceFound = false;
         try
         {
            sourceFound = EventLog.SourceExists(EVENT_SOURCE_NAME);
         }
         catch (System.Security.SecurityException)
         {
            sourceFound = false;
         }
         return sourceFound;
      }
   }
}
